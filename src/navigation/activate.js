import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container, Content, Button, Text,
  Form, View
} from 'native-base';
import { Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import Toast, { DURATION } from 'react-native-easy-toast';
import { resetLoging, activeredux, resendredux, LogoutToken } from '../service/actions';
import styles from '../styles/forms';
import renderInput from '../Components/renderInputs';

let MyInput;

class activate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      token: this.props.user.token,
      code: ''
    };
    MyInput = renderInput.bind(this);
  }

  componentDidMount() {
    console.log('Did Mount LogiAuth');
    this.props.resetLoging(null);
    if (this.props.user.token.active === 'yes') {
      console.log('Logging Activate');
      Actions.auth({ type: 'reset' });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user.token.active === 'yes') {
      console.log('Logging Activate');
      Actions.auth({ type: 'reset' });
    }
  }

  update = (prop, value) => {
    this.setState({
      [prop]: value
    });
  }

  renderButton() {
    if (this.props.user.logging !== undefined) {
      if (this.props.user.logging === true) {
        return (
          <Image
            source={require('../assets/images/loading.gif')}
            style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
          />
        );
      }
    }
    return (
      <Button
        Light rounded block style={styles.buttonStyle}
        onPress={() => this.props.submitin(this.state)}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} >  تاكيد </Text>
      </Button>
    );
  }

  renderResend() {
    if (this.props.user.send !== undefined) {
      if (this.props.user.send === true) {
        return (
          <Image
            source={require('../assets/images/loading.gif')}
            style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
          />
        );
      }
    }
    return (
      <Button
        transparent rounded block style={styles.buttonStyleTransperent}
        onPress={() => {
          this.props.resendredux(this.props.user.token);
          this.refs.toast.show(<View><Text>'جار ارسال الكود'</Text></View>, 500);
        }}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'black' }} >
          اعادة ارسال رمز التحقق مرة اخري
          </Text>
      </Button>
    );
  }

  render() {
    return (
      <Container isRTL style={{ backgroundColor: '#E9EAEB' }} >
        <Content keyboardShouldPersistTaps={'handled'} padder style={styles.content} >
          <Text style={styles.titleStyle} >  قم بادخال رمز التاكيد </Text>
          <Form style={styles.container}>
            <MyInput
              key='1' name={'code'} im='phone'
              label='رمز التاكيد'
              value={this.state.code}
              update={this.update}
              kp='numeric'
              req
            />
            <Text
              style={{ color: 'red', alignContent: 'center', flex: 1, textAlign: 'right' }}
            >{this.props.user.errors.message}</Text>
            {this.renderButton()}
          </Form>
          {this.renderResend()}
          <Button
            transparent rounded block style={styles.buttonStyleTransperent}
            onPress={() => {
              this.props.LogoutToken();
              Actions.Loginauth({ type: 'reset' });
            }
            }

          >
            <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'black' }} >
              تسجيل الدخول بحساب اخر
          </Text>
          </Button>
          <Toast ref="toast" />
        </Content>
      </Container >
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.users
  };
}

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    submitin: activeredux,
    resetLoging,
    resendredux,
    LogoutToken
  }, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(activate);
