import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Button, Text, View, Container, Content, Thumbnail } from 'native-base';
import styles from '../styles/forms';

class ToRegister extends Component {
  
  componentWillMount() {
    console.log('ToRegister Props', this.props.users);
    if (this.props.users.logged === true) {
      console.log('ToRegister User Has Token');
      Actions.auth({ type: 'reset' });
    }
  }

  render() {
    return (
      <Container isRTL >
        {/* <HeaderMain title="##قطعكم##" /> */}
        <View style={{ flex: 1, backgroundColor: '#E9EAEB' }} >
          <Content padder style={styles.content} >
            <Text style={styles.titleStyle} > اختر نوع الحساب المناسب </Text>
            <View style={{ flex: 1, }}>
              <Button
                full large rounded style={styles.MultibtnStyle}
                onPress={() => Actions.Register({ types: 'Individual' })}
              //onPress={() => this.ping()}
              >
                <Text style={styles.buttonText}>تسجيل عميل جديد </Text>
                {/* <Icon style={styles.iconStyleBig} type="FontAwesome" name='user-o' /> */}
                <Thumbnail square source={require('../assets/images/clint.png')} style={styles.iconImageBig} />

              </Button>
              <Button
                full large rounded style={styles.MultibtnStyle}
                onPress={() => Actions.Register({ types: 'Traders' })}
              >
                <Text style={styles.buttonText}>تسجيل تاجر </Text>
                {/* <Icon style={styles.iconStyleBig} type="MaterialCommunityIcons" name='worker' /> */}
                <Thumbnail square source={require('../assets/images/mech.png')} style={styles.iconImageBig} />

              </Button>
              <Button
                full large rounded style={styles.MultibtnStyle}
                onPress={() => Actions.Register({ types: 'Workshop' })}
              >
                <Text style={styles.buttonText}>تسجيل صاحب ورشة </Text>
                {/* <Icon style={styles.iconStyleBig} type="FontAwesome" name='car' /> */}
                <Thumbnail square source={require('../assets/images/carsett.png')} style={styles.iconImageBig} />
              </Button>
            </View>
          </Content>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users
});

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(ToRegister);
