import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container, Content, Button, Text,
  Form, Spinner, Thumbnail, View
} from 'native-base';
import { Image, Dimensions } from 'react-native';
import { bindActionCreators } from 'redux';
import DialogManager,
{
  DialogComponent, SlideAnimation,
  ScaleAnimation, DialogContent
}
  from 'react-native-dialog-component';
import { Actions } from 'react-native-router-flux';
import { resetLoging, loginuserredux, forgetredux } from '../service/actions';
import styles from '../styles/forms';
import renderInput from '../Components/renderInputs';

let MyInput;

const { width, height } = Dimensions.get('window');

class Loginauth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: [],
      typear: '',
      loaded: false,
      forg: false,
    };
    MyInput = renderInput.bind(this);
  }

  componentWillMount() {
    if (this.props.user.logged !== undefined) {
      console.log('Will Mount LogiAuth', this.props);
      if (this.props.user.logged === true) {
        clearInterval(this.intervalId);
        console.log('Logging User Has Token');
        if (this.props.user.token.active === 'no') {
          console.log('Logging NoActive');
          Actions.activate({ type: 'reset' });
        }
        Actions.auth({ type: 'reset' });
      }
    }
  }

  componentDidMount() {
    console.log('Did Mount LogiAuth');
    this.props.resetLoging(null);
  }

  componentWillReceiveProps(next) {
    if (next.user.errors.alertOK !== undefined) {
      if (next.user.errors.alertOK === true) {
        this.setState({ forg: false });
      }
    }
  }

  componentDidUpdate() {
    if (this.props.user.logged !== undefined) {
      if (this.props.user.logged === true) {
        console.log('Logging User Has Token');
        if (this.props.user.token.active === 'no') {
          console.log('Logging NoActive');
          Actions.activate({ type: 'reset' });
        } else {
          Actions.auth({ type: 'reset' });
        }
      }
    }
  }

  update = (prop, value) => {
    this.setState({
      [prop]: value
    });
  }

  dialog() {
    return (
      <DialogContent>
        <View style={{ marginTop: 0, }} >
          <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, alignSelf: 'center', }}>
            استعادة كلمة المرور

            </Text>
          <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 16, alignSelf: 'center', }}>
            من فضلك ادخل رقم جوالك لتتلقي رسالة تشتمل كلمة المرور الخاصة باشتراكك
						</Text>
          <View style={{ flex: 1, }} >
            <Form >
              <MyInput
                key='1' name={'user_phone'} im='phone'
                label='رقم الجوال'
                value={this.state.user_phone}
                update={this.update}
                type='phone'
                kp='numeric'
                // containerStyle={{ flex: 1 }}
                req
              />
              <Text
                style={{ color: 'red', alignContent: 'center', flex: 1, textAlign: 'right' }}
              >{this.props.user.errors.message}</Text>
              {this.renderButtonSend()}
            </Form>
          </View>
        </View>
      </DialogContent>
    );
  }

  renderButton() {
    if (this.props.user.logging !== undefined) {
      if (this.props.user.logging === true) {
        return (
          <Image
            source={require('../assets/images/loading.gif')}
            style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
          />
        );
      }
    }
    return (
      <Button
        Light rounded block style={styles.buttonStyle}
        onPress={() => this.props.submitin(this.state)}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > تسجيل دخول</Text>
      </Button>
    );
  }

  renderButtonSend() {
    if (this.props.user.send !== undefined) {
      if (this.props.user.send === true) {
        return (
          <Image
            source={require('../assets/images/loading.gif')}
            style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
          />
        );
      }
    }
    return (
      <Button
        Light rounded block style={styles.buttonStyle}
        onPress={() => this.props.forget(this.state)}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > ارسال كلمة المرور</Text>
      </Button>
    );
  }

  render() {
    return (
      <Container isRTL style={{ backgroundColor: '#E9EAEB' }} >
        <DialogComponent
          show={this.state.forg}
          width={width - 10}
          height={380}
          dialogStyle={[styles.dialog, { backgroundColor: '#fff', }]}
          ref={(dialogComponent) => { this.dialogComponent = dialogComponent; }}
          // dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
          onDismissed={() => {
            this.setState({ forg: false });
          }}
        >
          {this.dialog()}
        </DialogComponent>
        <Content keyboardShouldPersistTaps={'handled'} padder style={styles.content} >

          <Text style={styles.titleStyle} > تسجيل الدخول </Text>
          <Form style={styles.container}>

            <MyInput
              key='1' name={'user_phone'} im='phone'
              label='5xxxxxxxxxx'
              value={this.state.user_phone}
              update={this.update}
              kp='numeric'
              type='phone'
              req
            />

            <MyInput
              key='2' name={'passwordP'} im='lock'
              label='كلمة المرور' secure
              value={this.state.passwordP}
              update={this.update}
              req
              last
            />
            <Text
              style={{ color: 'red', alignContent: 'center', flex: 1, textAlign: 'right' }}
            >{this.props.user.errors.message}</Text>
            {this.renderButton()}
          </Form>
          <Button
            transparent rounded block style={styles.buttonStyleTransperent}
            onPress={() => Actions.ToRegister()}
          >
            <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'black' }} > اذا لم يكن لك حساب يالفعل الرجاء
          <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'red' }} >  تسجيل من هنا</Text>
            </Text>
          </Button>
          <Button
            transparent rounded block style={styles.buttonStyleTransperent}
            onPress={() => this.setState({ forg: true })}
          >
            <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'black' }} > نسيت كلمة المرور
            </Text>
          </Button>
        </Content>
        {/* </KeyboardAvoidingView> */}
      </Container >
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.users
  };
}

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    submitin: loginuserredux,
    resetLoging,
    forget: forgetredux
  }, dispatch, getState);
};

//Register = connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Register);
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Loginauth);
