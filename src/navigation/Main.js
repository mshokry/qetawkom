import React, { Component } from 'react';
import {
  AsyncStorage, Image, AlertIOS,
  Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Button, Text, View, Container,
  Content, Thumbnail,
  Drawer
} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';

import styles from '../styles/forms';
import {
  LogoutToken, getSettingRedux,
  myinforedux
} from '../service/actions';
import FooterMain from '../Components/footer';
import MainUser from '../Components/MainUser';
import MainTrade from '../Components/MainTrade';
import MainImport from '../Components/MainImport';
import Sidebar from './side';

class Main extends Component {

  componentWillMount() {
    if (this.props.users.token === null) {
      console.log('MAIN User not logged Will');
      Actions.Reg({ type: 'reset' });
    } else {
      if (this.props.users.token.active === 'no') {
        console.log('Logging NoActive');
        Actions.activate({ type: 'reset' });
      }
      console.log('Main Component Updates USER !!', this.props.users);
      console.log('Main Component Updates TOKEN ID !!', this.props.users.token.ID);
      console.log('Main Component Updates PARTS !!', this.props.parts.settings);
      if (!(Object.keys(this.props.parts.settings).length > 0)) {
        const { token } = this.props.users;
        this.props.getSetting(token);
      }
    }
  }
  /*
    componentWillReceiveProps(nextProps) {
      if (nextProps.users.token === null || nextProps.parts.logout === true) {
        console.log('MAIN User not logged did');
        Actions.Reg({ type: 'reset' });
      }
      if (this.props.users.token.active === 'no') {
        console.log('Logging NoActive');
        Actions.activate({ type: 'reset' });
      }
    }*/

  async storeData(data) {
    try {
      await AsyncStorage.setItem('token', JSON.stringify(data));
      console.log('storeData saved');
    } catch (error) {
      // Error saving data
      console.log('storeData Not saved');
    }
  };

  async logout() {
    if (Platform.OS === 'ios') {
      //Clear Token
      AlertIOS.alert(
        'تسجيل الخروج',
        'هل تود الخروج ؟',
        [
          {
            text: 'تسجيل خروج',
            onPress: () => {
              console.log('Logging out()');
              this.props.LogoutToken(null);
              this.storeData({});
              AsyncStorage.removeItem('token').then(() => {
                this.props.LogoutToken(null);
                console.log('Logged out');
                Actions.Loginauth({ type: 'reset' });
              });
            },
          },
          {
            text: 'الغاء',
            onPress: () => console.log('Install Pressed'),
            style: 'cancel',
          },
        ]
      );
    } else {
      console.log('Logging out()');
      this.props.LogoutToken(null);
      AsyncStorage.removeItem('token').then(() => {
        console.log('Logged out');
        Actions.Loginauth({ type: 'reset' });
      });
    }
  }

  closeDrawer = () => {
    this.drawer._root.close();
  };

  openDrawer = () => {
    this.drawer._root.open();
  };

  renderMain() {
    if (this.props.users.token === null) {
      return (<View />);
    }
    if (this.props.users.token.type === 'trader') {
      return (<MainTrade user={this.props.users.token} logout={this.logout.bind(this)} />);
    }
    if (this.props.users.token.type === 'importer') {
      return (<MainImport user={this.props.users.token} logout={this.logout.bind(this)} />);
    }
    if (this.props.users.token.type === 'workshop') {
      return (<MainUser user={this.props.users.token} logout={this.logout.bind(this)} />);
    }
    return (<MainUser user={this.props.users.token} logout={this.logout.bind(this)} />);

  }

  render() {
    console.log('Main State ', this.state);
    console.log('Main Props', this.props);
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<Sidebar navigator={this.navigator} />}
        onClose={() => this.closeDrawer()}
      >
        <Container isRTL >
          <View style={{ flex: 1, backgroundColor: '#E9EAEB' }} >
            <Content padder style={styles.content} >
              <Spinner
                visible={this.props.parts.loading === null ? false : this.props.parts.loading}
                textContent={'جاري التحميل ...'} textStyle={{ color: '#0071D0' }}
              />
              <Text style={styles.titleEmptyStyle} />
              {this.renderMain()}
            </Content>
          </View>

          <FooterMain />
        </Container>
      </Drawer>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
  parts: state.parts
});

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    LogoutToken,
    getSetting: getSettingRedux,
    myinforedux,
  }, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Main);
