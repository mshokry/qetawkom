import React, { Component } from 'react';
import { Dimensions, Image, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Button, Text, View, Container,
  Content, ListItem, Body, Badge,
  Form, Thumbnail, Item, Input
} from 'native-base';
import DialogManager,
{
  DialogComponent, SlideAnimation,
  ScaleAnimation, DialogContent,
}
  from 'react-native-dialog-component';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import renderInput from '../Components/renderInputs';
import styles from '../styles/forms';
import { addOfferRedux, resetAlert } from '../service/actions';

const { width, height } = Dimensions.get('window');
let MyInput;

class RequestsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      forg: false,
      ID: this.props.ID,
      token: this.props.users.token
    };
    MyInput = renderInput.bind(this);
  }

  type(t) {
    switch (t) {
      case 'used':
        return ('طلب قطعة غيار مستعملة');
      case 'new':
        return ('طلب قطعة غيار مستعملة');
      default:
        return ('طلب قطعة غيار جديدة');
    }
  }

  update = (prop, value) => {
    //console.log(prop, value);
    this.setState({
      [prop]: value
    });
  }

  pickerShow = () => {
    // iPhone/Android
    DocumentPicker.show({
      filetype: [DocumentPickerUtil.images()],
      // filetype: [DocumentPickerUtil.images()],
    }, (error, res) => {
      console.log("Error", error);
      if (error === null) {
        console.log(
          res.uri,
          res.type, // mime type
          res.fileName,
          res.fileSize
        );
        this.setState({
          image: res,
          imageok: true
        });
      } else {
        this.setState({
          image: {},
          imageok: false
        });
      }
    });
  };


  dialog() {
    return (
      <DialogComponent
        show={this.state.forg}
        height={height - 80}
        dialogStyle={[styles.dialog, { backgroundColor: '#fff', }]}
        ref={(dialogComponent) => { this.dialogComponent = dialogComponent; }}
        // dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
        onDismissed={() => {
          this.setState({ forg: false });
          this.props.resetAlert(false);
          //Actions.pop();
        }}
      >

        <DialogContent>
          {/* <View style={{ alignSelf: 'flex-start', alignItems: 'center', marginTop: 0, flex: 1, }} > */}
          <View style={{ marginTop: 0, }} >
            <Form style={styles.container}>
              <MyInput
                key='1' name={'Price'} im='price'
                label='السعر'
                value={this.state.Price}
                update={this.update}
                kp='numeric'
                req
              />
              <MyInput
                key='2' name={'DeliveryTime'} im='date'
                label='وقت التسليم'
                value={this.state.DeliveryTime}
                update={this.update}
                req
              />
              <MyInput
                keyx='4' name={'Specifications'} im='note'
                label={'وصف السلعة'} type='multi'
                containerStyle={{ height: 100 }}
                update={this.update}
                value={this.state.Specifications}
              />
              <Item
                onPress={() => this.pickerShow()}
                rounded style={styles.item}
              >
                {
                  this.state.imageok ?
                    (<Thumbnail square source={{ uri: this.state.image.uri }} />)
                    : (<View />)
                }
                <Input
                  keyx='20' name={'image'} im='lock'
                  disabled
                  label='ارفاق الاستمارة'
                  style={styles.inputStyle}
                  placeholder={'ارفاق الصورة'}
                />

                <Thumbnail
                  square
                  source={require('../assets/images/camera.png')}
                  style={styles.iconImageBig}
                />
              </Item>
              <Text
                style={{ color: 'red', alignContent: 'center', flex: 1, textAlign: 'right' }}
              >{this.props.parts.errors.message}</Text>
              {this.renderButton()}
            </Form>
          </View>
        </DialogContent>

      </DialogComponent >
    );
  }

  renderButton() {
    if (this.props.users.logging !== undefined) {
      if (this.props.users.logging === true) {
        return (
          <Image
            source={require('../assets/images/loading.gif')}
            style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
          />
        );
      }
    }
    return (
      <Button
        Light rounded block style={styles.buttonStyle}
        onPress={() => this.props.offer(this.state)}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > ارسال عرضك للعميل </Text>
      </Button>
    );
  }

  renderOffers() {
    const { Offers } = this.props.item;
    if (Offers) {
      Offers.map((key) => {
        console.log(key);
        return (
          <View>
            <ListItem>
              <Body>
                <Text style={styles.baseText}>طلب عميل </Text>
                <Text note style={styles.noteTextRed} >تاريخ 5/55/55555</Text>
                <Text note style={styles.noteText} > رقم : 5435353534 </Text>
              </Body>
              <Image style={styles.ImageStyle} source={require('../assets/images/car3.jpg')} />
            </ListItem>
          </View>
        );
      });
    } else {
      return (<View />);
    }
  }

  renderUser(item) {
    if (this.props.users.token.type === 'user' || this.props.users.token.type === 'workshop') {
      const { name, ID } = item;
      return (
        <View>
          <View style={styles.PartV}>
            <View style={{ height: 3, width: 30, backgroundColor: '#000' }} />
            <Text style={[styles.PartT, { textAlign: 'center' }]}> العروض المقدمة </Text>
          </View>

          <FlatList
            style={{ width, marginTop: 20, }}
            data={item.Offers}
            renderItem={({ item, index }) => {
              return (
                <ListItem
                  key={item.ID} onPress={() => Actions.Offer({ item, name, ID })}
                  style={{ backgroundColor: index % 2 ? '#E9EAEB' : 'lightgray', flex: 1, }}
                >
                  <Body >
                    <Text style={styles.baseText}>{item.UserName ? item.UserName : 'بدون عنوان'} </Text>
                    <View style={{ flexDirection: 'row-reverse', }} >
                      <Image
                        style={{ width: 80, height: 80 }}
                        mode='stretch'
                        source={{ uri: `http://qeta3km.a5aa.com/uploads/${item.photo}` }}
                      />
                      <View style={{ flex: 9 }}>
                        <Text note style={[styles.noteTextRed, {}]} > السعر {item.Price}</Text>
                        <Text note style={[styles.noteText, { fontSize: 14 }]} > تاريخ العرض {item.add_date}</Text>
                      </View>
                      {item.OffersCount ?
                        (<Badge
                          style={{ flex: 2, backgroundColor: '#E9EAEB', height: 40, alignContent: 'center', marginLeft: 20, }}
                        >
                          <Text style={[styles.baseText, { marginTop: 12, }]}>
                            {item.OffersCount} عرض</Text>
                        </Badge>)
                        :
                        (<View />)
                      }
                    </View>
                  </Body>
                </ListItem>
              );
            }
              // </TouchableWithoutFeedback>
            }
            keyExtractor={(item, index) => item.ID.toString()}
          />
        </View>
      );
    }

    if (this.props.users.logging !== undefined) {
      if (this.props.users.logging === true) {
        return (
          <Image
            source={require('../assets/images/loading.gif')}
            style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
          />
        );
      }
    }

    return (
      <Button
        Light rounded block style={styles.buttonStyle}
        onPress={() => this.setState({ forg: true })}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > ارسال عرضك للعميل </Text>
      </Button>
    );
  }

  render() {
    const { item } = this.props;
    console.log(item);
    const { PartV, PartD, PartT } = styles;
    return (
      <Container
        style={{ flex: 1, backgroundColor: '#E9EAEB', width }}
        contentContainerStyle={{ flexGrow: 1 }}
      >
        {this.dialog()}

        <DialogComponent
          show={this.props.parts.alertOK}
          width={280}
          height={220}
          dialogStyle={styles.dialog}
          ref={(dialogComponent1) => { this.dialogComponent1 = dialogComponent1; }}
          // dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
          onDismissed={() => {
            this.props.resetAlert(false);
            Actions.pop();
          }}
        >
          <DialogContent>
            <View style={{ marginTop: 0, }} >
              <Thumbnail
                square
                source={require('../assets/images/check.png')}
                style={[styles.iconImageBig, { tintColor: '#fff' }]}
              />
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 22, alignSelf: 'center', }}>

                تم ارسال طلبك للعميل

            </Text>
            </View>
          </DialogContent>
        </DialogComponent>

        <View style={{ flex: 1, backgroundColor: '#E9EAEB', width, marginLeft: -10, }} >
          <Text style={styles.titleStyle} > تفاصيل الطلب </Text>
          <Content padder style={[styles.content, { width: '100%', }]} >
            {item.name ? (<View style={PartV}><Text style={PartT}>{item.name}</Text></View>) : (<View />)}
            {item.add_date ? (<View style={PartV}><Text style={[styles.noteText, { color: '#89898A' }]}>تاريخ الاعلان : {item.add_date}</Text></View>) : (<View />)}
            {item.ID ? (<View style={PartV}><Text style={[styles.noteText, { color: '#89898A', marginBottom: 20, }]}>رقم الاعلان : {item.ID}</Text></View>) : (<View />)}
            {item.type ? (<View style={PartV}><Text style={PartT}>مطلوب :</Text><Text style={PartD}>{this.type(item.type)}</Text></View>) : (<View />)}
            {item.car ? (<View style={PartV}><Text style={PartT}>ماركة السيارة :</Text><Text style={PartD}>{item.car}</Text></View>) : (<View />)}
            {item.model ? (<View style={PartV}><Text style={PartT}>نوع السيارة :</Text><Text style={PartD}>{item.model}</Text></View>) : (<View />)}
            {item.make_year ? (<View style={PartV}><Text style={PartT}>الموديل :</Text><Text style={PartD}>{item.make_year}</Text></View>) : (<View />)}
            {item.ImportCountry ? (<View style={PartV}><Text style={PartT}>الدولة :</Text><Text style={PartD}>{item.ImportCountry}</Text></View>) : (<View />)}
            {item.name ? (<View style={PartV}><Text style={PartT}>أسم القطعة :</Text><Text style={PartD}>{item.name}</Text></View>) : (<View />)}
            {item.FormNumber ? (<View style={PartV}><Text style={PartT}>رقم الإستمارة :</Text><Text style={PartD}>{item.FormNumber}</Text></View>) : (<View />)}
            {this.renderUser(item)}
          </Content>
        </View>
      </Container >
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
  parts: state.parts
});

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    offer: addOfferRedux,
    resetAlert
  }, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(RequestsDetail);
