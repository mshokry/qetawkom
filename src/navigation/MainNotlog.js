import React, { Component } from 'react';
import { AsyncStorage, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Text, View, Container, Content, Thumbnail } from 'native-base';
import { loginuser, retrieveData } from '../service/actions';
import styles from '../styles/forms';

class MainNotlog extends Component {
  constructor(props) {
    super();
    this.state = {
      visible: true,
      loaded: false
    };
  }
/*
  async componentWillMount() {
    console.log('Will mount MainNotlog');
    this.setState({ loading: false });

    if (this.props.users.logged === true) {
      console.log('Logging User Has Token');
      Actions.auth({ type: 'reset' });
    } else {
      console.log('Checking Storage');
      try {
        const value = await AsyncStorage.getItem('token');
        if (value !== null) {
          // We have data!!
          console.log('Storage conatins Token', value);
          this.props.loginuser(JSON.parse(value));
          if (this.props.users.logged === true) {
            Actions.auth({ type: 'reset' });
          }
        }
      } catch (error) {
        // Error retrieving data
        console.log('error loading data', error);
      }
      console.log('### Storage Main OR Splash ###');
    }
  }
*/
  componentDidMount() {
    console.log('did mount MainNotlog', this.props.users);
    if (this.state.loaded) {
      this.setState({ visible: false });
    }
    if (this.props.users.logged === true) {
			clearInterval(this.intervalId);
      console.log('Logging User Has Token Main');
			if (this.props.users.token.active === 'no') {
      console.log('Logging NoActive');
        Actions.activate({ type: 'reset' });
			} else {
        Actions.auth({ type: 'reset' });
      }
		}
  }

  render() {
    return (
      <Container isRTL >
        {/* <HeaderMain title="##قطعكم##" /> */}
        <View style={{ flex: 1, backgroundColor: '#E9EAEB' }} >
          <Content padder style={styles.content} >
            <Text style={styles.titleEmptyStyle} />
            <View style={{ flex: 1, }}>
              <Button
                full large rounded style={styles.MultibtnStyle}
                onPress={() => Actions.Loginauth({ types: 'Workshop' })}
              >
                <Text style={styles.buttonText}> طلب قطعة غيار جديده </Text>
                <Thumbnail square source={require('../assets/images/set.png')} style={styles.iconImageBig} />
              </Button>

              <Button
                full large rounded style={styles.MultibtnStyle}
                onPress={() => Actions.Loginauth({ types: 'Individual' })}
              >
                <Text style={styles.buttonText}> طلب قطعة غيار مستعملة </Text>
                <Thumbnail square source={require('../assets/images/tsettin.png')} style={styles.iconImageBig} />
              </Button>
              <Button
                full large rounded style={styles.MultibtnStyle}
                onPress={() => Actions.Loginauth({ types: 'Traders' })}
              >
                <Text style={styles.buttonText}> طلب قطعة غيار دراجة نارية </Text>
                <Thumbnail square source={require('../assets/images/motorcycle.png')} style={styles.iconImageBig} />
              </Button>
              <Button
                full large rounded style={styles.MultibtnStyle}
                onPress={() => Actions.Loginauth({ types: 'Workshop' })}
              >
                <Text style={styles.buttonText}> إستيراد قطعة غيار </Text>
                <Thumbnail square source={require('../assets/images/plane.png')} style={styles.iconImageBig} />
              </Button>
            </View>
          </Content>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users
});

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    loginuser,
    retrieveData
  }, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(MainNotlog);
