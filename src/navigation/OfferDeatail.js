import React, { Component } from 'react';
import { Dimensions, Image, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Button, Text, View, Container,
  Content
} from 'native-base';
// import MapView from 'react-native-maps';
import styles from '../styles/forms';
import { addOfferRedux, resetAlert } from '../service/actions';

const { width, height } = Dimensions.get('window');

class OfferDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      forg: false,
      ID: this.props.ID,
      token: this.props.users.token
    };
  }

  render() {
    const { item, name, ID } = this.props;
    console.log(item);
    const { PartV, PartD, PartT } = styles;
    return (
      <Container
        style={{ flex: 1, backgroundColor: '#E9EAEB', width }}
        contentContainerStyle={{ flexGrow: 1 }}
      >
        <View style={{ flex: 1, backgroundColor: '#E9EAEB', width, marginLeft: -10, }} >
          <Text style={styles.titleStyle} > تفاصيل العرض </Text>
          <Content padder style={[styles.content, { width: '100%', }]} >
            <Text style={styles.baseText}>{item.UserName ? item.UserName : 'بدون عنوان'} </Text>
            <Text note style={[styles.noteTextRed, {}]} > {item.Price} ريال</Text>
            <Text note style={[styles.noteText, { fontSize: 12 }]} > تاريخ العرض {item.add_date}</Text>

            {name ? (<View style={PartV}><Text style={PartT}>{name}</Text></View>) : (<View />)}
            {ID ? (<View style={PartV}><Text style={[styles.noteText, { color: '#89898A', marginBottom: 20, }]}>رقم الاعلان : {ID}</Text></View>) : (<View />)}
            {item.Specifications ? (<View style={PartV}><Text style={PartT}>وصف السلعه :</Text><Text style={PartD}>{item.Specifications}</Text></View>) : (<View />)}
            {item.DeliveryTime ? (<View style={PartV}><Text style={PartT}>شروط التسليم :</Text><Text style={PartD}>{item.DeliveryTime}</Text></View>) : (<View />)}
            <Image
              style={{ width: width - 180, height: width - 180, alignSelf: 'center', }}
              mode='stretch'
              source={{ uri: `http://qeta3km.a5aa.com/uploads/${item.photo}` }}
            />

            {/* <MapView
              initialRegion={{
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
            />  */}

            <Button
              Light rounded block style={[styles.buttonStyle, { backgroundColor: 'green' }]}
              onPress={() => {
                const url = `whatsapp://send?text=${`${item.ID} ${item.Specifications}`}&phone=${item.UserPhone}`;
                Linking.canOpenURL(url).then(supported => {
                  if (!supported) {
                    console.log('Can\'t handle url: ' + url);
                  } else {
                    Linking.openURL(url);
                  }
                })
                  .catch(err => console.error('An error occurred', err));
              }
              }
            >
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > مراسلة عبر الواتس </Text>
            </Button>
            <Button
              Light rounded block style={styles.buttonStyle}
              onPress={() => {
                const url = `tel://${item.UserPhone}`;
                Linking.openURL(url);
              }}
            >
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > اتصال عبر الهاتف </Text>
            </Button>
          </Content>
        </View>
      </Container >
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
  parts: state.parts
});

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    offer: addOfferRedux,
    resetAlert
  }, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(OfferDetail);
