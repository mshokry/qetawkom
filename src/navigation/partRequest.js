import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
	Image, TouchableOpacity,
	TouchableHighlight, TouchableWithoutFeedback
} from 'react-native';

import {
	Container, Content, Button, Text,
	Form, Spinner, Item, Picker, Input, View, Thumbnail
} from 'native-base';
import { bindActionCreators } from 'redux';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import { Actions } from 'react-native-router-flux';
import DialogManager,
{
	DialogComponent, SlideAnimation,
	ScaleAnimation, DialogContent
}
	from 'react-native-dialog-component';

import ModalDropdown from 'react-native-modal-dropdown';

import { resetLoading, addPartRedux, resetAlert } from '../service/actions';
import styles from '../styles/forms';
import Title from '../Components/partRequestTitle';

class partRequest extends Component {
	constructor(props) {
		super(props);
		const today = new Date();
		const year = today.getFullYear();
		this.state = {
			image: {},
			location: '',
			imageok: false,
			typear: '',
			loaded: false,
			Cars: this.props.parts.settings.Cars,
			selected: this.props.parts.settings.Cars[0],
			selectedSub: this.props.parts.settings.Cars[0].Models[0],
			selectedID: 0,
			selectedYear: `${year}`,
			year,
			type: this.props.types,
			import_image: this.props.image,
			token: this.props.user.token,
			text: '',
		};
		this.renderSubListR = this.renderSubList.bind(this, true);
	}

	componentWillMount() {
		console.log("Will Mount");
		
		this.props.resetLoading(null);
		if (!(Object.keys(this.props.parts.settings).length > 0)) {
			Actions.pop();
		}
	}

	componentDidMount() {
		console.log("DID Mount");

		const items = this.props.parts.settings.Cars;
		const type = this.state.type;
		const data = [];
		if (type === 'new') {
			items.map((val) => {
				if (val.type === '1' || val.type === '3') { //if Car
					data.push(val);
				}
			});
			this.setState({ Cars: data, selected: data[0] });
		}
		if (type === 'motor') {
			items.map((val) => {
				if (val.type === '2' || val.type === '3') { //if Car
					data.push(val);
				}
			});
			this.setState({ Cars: data, selected: data[0] });
		}
	}

	renderListImage(items, rowID, highlighted) {
		const icon = 'http://qeta3km.a5aa.com/uploads/';
		return (
			<TouchableHighlight underlayColor='cornflowerblue'>
				<View
					style={{
						color: '#C4C4C4',
						height: 55,
						alignContent: 'center',
						alignSelf: 'center',
						alignItems: 'center',
						flexDirection: 'row-reverse',
						width: '85%'
					}}
				>
					<Image
						style={{ width: 40, height: 40, paddingRight: 30, }}
						mode='stretch'
						source={{ uri: `${icon}${items.photo}` }}
					/>
					<Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, }}>
						{`${items.name}`}
					</Text>
				</View>
			</TouchableHighlight>
		);
	}

	renderList(items) {
		const data = [];
		items.map((val) => {
			if (val.type === '1' || val.type === '3') { //if Car
				data.push(val);
			}
		});
		return data.map((val, key) => {
			return (
				<Picker.Item
					label={val.name}
					value={val} key={key}
					style={styles.inputStyle}
				/>
			);
		});
	}

	renderButton() {
		if (this.props.parts.loading !== undefined) {
			if (this.props.parts.loading === true) {
				return (
					<Image
              source={require('../assets/images/loading.gif')}
              style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
					/>
				);
			}
		}
		return (
			<Button
				Light rounded block style={styles.buttonStyle}
				onPress={() => this.props.submitin(this.state)}
			>
				<Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} >  ارسال الطلب للتاجر </Text>
			</Button>
		);
	}

	renderSubList(items, st) {
		if (items === undefined) {
			this.setState({ selectedSub: '0' });
			return (
				<Picker.Item label='لا يوجد بيانات' selected value={'0'} key={0} />
			);
		}
		const data = [];
		
		if (this.state.type === 'motor') {
			items.map((val) => {
				if (val.type === '2' || val.type === '3') { //if Car
					data.push(val);
				}
			});
		} else {
			items.map((val) => {
				if (val.type === '1' || val.type === '3') { //if Car
					data.push(val);
				}
			});
		}
		if (st === true) {
			this.setState({ selectedSub: items[0] });
		} else {
		return data.map((val, key) => {
			return (
				<Picker.Item label={val.name} value={val} key={key} style={styles.inputStyle} />
			);
		});
		}
	}

	renderYear() {
		const years = [];
		if (this.state.selectedYear === 0) {

		}
		for (let i = 0; i < 20; i++) {
			years.push([this.state.year - i]);
		}
		return years.map((val) => {
			return (
				<Picker.Item label={`${val}`} selected={val === this.state.selectedYear} value={`${val}`} key={`${val}`} style={styles.inputStyle} />
			);
		});
	}

	render() {
		const { selected, } = this.state;
		const { Cars } = this.props.parts.settings;
		console.log('partRequest State ', this.state);
		console.log('partRequest Props', this.state);

		return (
			<Container isRTL style={{ backgroundColor: '#E9EAEB' }} >

				<DialogComponent
					show={this.props.parts.alertOK}
					width={280}
					height={220}
					dialogStyle={styles.dialog}
					ref={(dialogComponent) => { this.dialogComponent = dialogComponent; }}
					// dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
					onDismissed={() => {
						this.props.resetAlert(false);
						Actions.pop();
					}}
				>
					<DialogContent>
						<View style={{ marginTop: 0, }} >
							<Thumbnail
								square
								source={require('../assets/images/check.png')}
								style={[styles.iconImageBig, { tintColor: '#fff' }]}
							/>
							<Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, alignSelf: 'center', }}>
								تم اضافة طلبك للتجار .. يمكنك رؤية العروض من قائمة طلباتي
						</Text>
						</View>
					</DialogContent>
				</DialogComponent>

				<Content padder style={styles.content} >
					<Title type={this.state.type} country={this.state.import_image} />
					<Form style={styles.container}>

						<Item Picker rounded style={styles.item} >
							<ModalDropdown
								ref={el => this.dropdown = el}
								options={this.state.Cars}
								defaultValue={'اختار'}
								style={[styles.item, { width: '90%', height: 60, alignSelf: 'center', borderRadius: 20, }]}
								textStyle={styles.baseText}
								dropdownStyle={{
									flex: 1,
									width: '85%',
									height: 220,
									borderRadius: 20,					
								}}
								onSelect={(value) => {
									this.setState({ selected: this.state.Cars[value], selectedID: value, selectedSub: undefined });
									//this.renderSubListR(this.state.Cars[value].Models);
								}}
								// renderButtonText={() => (selected.name)}
								renderRow={this.renderListImage.bind(this)}
							>
								<TouchableWithoutFeedback
									onPress={() => { this.dropdown && this.dropdown.show(); }}
									underlayColor='cornflowerblue'
								>
									<View style={styles.itemPick} >
										<Image
											style={{ width: 40, height: 40 }}
											mode='stretch'
											source={{ uri: `http://qeta3km.a5aa.com/uploads/${selected.photo}` }}
										/>
										<Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, }}>
											{`   ${selected.name}`}
										</Text>
									</View>
								</TouchableWithoutFeedback>
							</ModalDropdown>
						</Item>

						<Item Picker rounded style={styles.item} >
							<Picker
								itemStyle={styles.pickerStyle}
								itemTextStyle={styles.PicInputStyle}
								style={{ height: 50, flex: 1, flexDirection: 'row-reverse'}}
								name="model"
								mode="dropdown"
								iosHeader=" النوع"
								selectedValue={this.state.selectedSub}
								onValueChange={(value) => this.setState({ selectedSub: value })}
							>
								{this.renderSubList(this.state.selected.Models)}
							</Picker>
						</Item>

						<Item Picker rounded style={styles.item} >
							<Picker
								itemStyle={styles.pickerStyle}
								style={{ height: 50, width: '100%', alignContent: 'flex-start', alignSelf: 'center', flex: 1, flexDirection: 'row-reverse'}}
								itemTextStyle={styles.PicInputStyle}
								name="make_year"
								mode="dropdown"
								iosHeader=" سنه الصنع"
								selectedValue={this.state.selectedYear}
								onValueChange={(value) => this.setState({ selectedYear: value })}
							>
								{this.renderYear()}
							</Picker>
						</Item>

						<Item regular rounded style={styles.item}>
							<Input
								key='1' name={'name'} im='phone'
								style={[styles.inputStyle, { borderColor: '#fff' }]}
								onChangeText={(value) => this.setState({ text: value })}
								placeholder='اسم القطعة'
							/>
						</Item>

						{
							this.state.type === 'new' || this.state.type === 'motor' ? (
								<Item
									onPress={() => {
										DocumentPicker.show({
											filetype: [DocumentPickerUtil.images()],
										}, (error, res) => {
											console.log("Error", error);
											if (error === null) {
												console.log(
													res.uri,
													res.type, // mime type
													res.fileName,
													res.fileSize
												);
												this.setState({
													image: res,
													imageok: true
												});
											} else {
												this.setState({
													image: {},
													imageok: false
												});
											}
										});
									}}
									regular rounded style={styles.item}
								>
									<Thumbnail
										square
										source={require('../assets/images/cloud.png')}
										style={styles.iconImageBig}
									/>
									{
										this.state.imageok ?
											(<Thumbnail square source={{ uri: this.state.image.uri }} />)
											: (<View />)
									}
									<Input
										key='2' name={'password'} im='lock'
										disabled
										label='ارفاق الاستمارة'
										style={[styles.inputStyle, { borderColor: '#FFF', flex: 2, }]}
									/>
									<Text style={[styles.subinputStyle, { flex: 9, }]}>
										ارفاق الاستمارة <Text style={{ color: 'red' }} >             (*) </Text>
									</Text>
								</Item>
							) : (<View />)
						}
						{
							this.state.type === 'new' ? (
								<Item regular rounded last style={styles.item}>
									<Input
										key='7' name={'number'} im='phone'
										style={[styles.inputStyle, { borderColor: '#fff' }]}
										onChangeText={(value) => this.setState({ number: value })}
										placeholder='رقم الاستمارة '
									/>
								</Item>
							) : (<View />)
						}
						{/* <Text style={{ color: 'red', alignContent: 'center', flex: 1 }} >
							{this.props.user.errors.message}
						</Text> */}

						{this.renderButton()}

					</Form>
				</Content >
			</Container >
		);
	}
}

function mapStateToProps(state) {
	return {
		parts: state.parts,
		user: state.users
	};
}

const mapDispatchToProps = (dispatch, getState) => {
	return bindActionCreators({
		submitin: addPartRedux,
		resetLoading,
		resetAlert
	}, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(partRequest);
