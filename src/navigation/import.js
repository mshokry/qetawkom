import React, { Component } from 'react';
import { Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Text, View, Container, Content, Thumbnail } from 'native-base';

import styles from '../styles/forms';

class Import extends Component {

	render() {
		return (
			<Container isRTL >
				{/* <HeaderImport title="##قطعكم##" /> */}
				<View style={{ flex: 1, backgroundColor: '#E9EAEB' }} >
					<Content padder style={styles.content} >

						<Text style={styles.titleEmptyStyle} />
						<View style={{ flex: 1, }}>

							<Button
								transparent full large block
								style={[styles.MultibtnStyle, { backgroundColor: '#E9EAEB', }]}
								onPress={() => Actions.partRequest({ types: 'import', image: 'uae' })}
							>
								<Image
									source={require('../assets/images/uaep.png')}
									style={{ resizeMode: 'contain', }}
								/>
							</Button>

							<Button
								transparent full large block
								style={[styles.MultibtnStyle, { backgroundColor: '#E9EAEB', }]}
								onPress={() => Actions.partRequest({ types: 'import', image: 'usa_flag' })}
							>
								<Image
									source={require('../assets/images/usap.png')}
									style={{ resizeMode: 'contain', }}
								/>

							</Button>

							<Button
								transparent full large block
								style={[styles.MultibtnStyle, { backgroundColor: '#E9EAEB', }]}
								onPress={() => Actions.partRequest({ types: 'import', image: 'japan' })}
							>
								<Image
									source={require('../assets/images/jpp.png')}
									style={{ resizeMode: 'contain', }}
								/>
							</Button>
						</View>
					</Content>
				</View>
			</Container>
		);
	}
}

const mapStateToProps = (state) => ({
	users: state.users,
	parts: state.parts
});

const mapDispatchToProps = (dispatch, getState) => {
	return bindActionCreators({

	}, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Import);
