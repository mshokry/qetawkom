import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container, Content, Button, Text, ListItem,
  Form, Spinner, View, Thumbnail, Item, Input, Right, Radio
} from 'native-base';
import { Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
// import RNGooglePlacePicker from 'react-native-google-place-picker';
import { adduserredux, resetLoging, myinforedux } from '../service/actions';
import styles from '../styles/forms';
import renderInput from '../Components/renderInputs';

let MyInput;

class myInfo extends Component {
  constructor(props) {
    super(props);
    // MyInput = MyInput.bind(this);
    //let identfy = this.props.type === 'trade' ? 'تاجر' : 'ورشة';
    // this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      image: {},
      loc: { latitude: undefined, longitude: undefined },
      imageok: false,
      typear: '',
      loaded: false,
      type: this.props.types,
      user_phone: '',
      name: '',
      password: '',

    };
    MyInput = renderInput.bind(this);
  }
  
  componentDidMount() {
    console.log('Component mounted');
    // this.props.reset();
    this.props.resetLoging(null);
    if (this.props.user.token !== null) {
      console.log('Logging User Has Token');
      Actions.auth();
    }
    this.props.myinforedux(this.props.user.token);
    console.log('Type ####', this.props.types);
    if (this.props.types === 'Traders') {
      this.setState({ typear: 'متجر', types: 'Trade' });
    } else if (this.props.types === 'Workshop') {
      this.setState({ typear: 'ورشة', types: 'Workshop' });
    } else {
      this.setState({ typear: 'جديد', types: 'Individual' });
    }
  }

  update = (prop, value) => {
    //console.log(prop, value);
    this.setState({
      [prop]: value
    });
  }

  locationPicker() {
    /*
    RNGooglePlacePicker.show((response) => {
      if (response.didCancel) {
        console.log('User cancelled GooglePlacePicker');
      } else if (response.error) {
        console.log('GooglePlacePicker Error: ', response.error);
      } else {
        console.log('GooglePlacePicker Place: ', response);
        this.setState({
          loc: response
        });
      }
    });*/
  }

  pickerShow = () => {
    // iPhone/Android
    DocumentPicker.show({
      filetype: [DocumentPickerUtil.images()],
    }, (error, res) => {
      console.log("Error", error);
      if (error === null) {
        console.log(
          res.uri,
          res.type, // mime type
          res.fileName,
          res.fileSize
        );
        this.setState({
          image: res,
          imageok: true
        });
      } else {
        this.setState({
          image: {},
          imageok: false
        });
      }
    });
  };

  renderButton() {
    if (this.props.user.logging !== undefined) {
      if (this.props.user.logging === true) {
        return (
          <Image
              source={require('../assets/images/loading.gif')}
              style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
          />
        );
      }
    }
    return (
      <Button
        Light rounded block style={styles.buttonStyle}
      onPress={() => this.props.submitin(this.state)}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > تسجيل جديد</Text>
      </Button>
    );
  }

  render() {
    const { latitude, longitude } = this.state.loc;

    const { typear } = this.state;
    //console.log('###Props###', this.props);
    //console.log('###state###', this.state);

    if (this.state.types === 'Individual') {
      return (
        <Container isRTL style={{ backgroundColor: '#E9EAEB' }} >

          <Content padder style={styles.content} >
            <Text style={styles.titleStyle} > تسجيل حساب جديد </Text>
            <Form
              style={styles.container}
            // onSubmit={(e) => { this.props.onSubmit }}
            >
              <MyInput
                keyx='3' name={'name'} im='lock' label='الاسم '
                value={this.state.name}
                update={this.update}
              />
              <MyInput
                keyx='1' name={'user_phone'} im='phone'
                label='رقم الجوال' update={this.update}
                kp='numeric'
                type='phone'
                value={this.state.user_phone}
              />
              <MyInput
                keyx='2' name={'password'} im='lock'
                label='كلمة المرور' secure update={this.update}
                value={this.state.password}
              />
              <Text style={{ color: 'red', alignContent: 'center', flex: 1 }} >{this.props.user.errors.message}</Text>
              {this.renderButton()}
            </Form>
            <Button
              transparent rounded block style={styles.buttonStyleTransperent}
              onPress={() => Actions.Loginauth()}
            >
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'black' }} > اذا كان لديك حساب يالفعل الرجاء
          <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'red' }} >  تسجيل الدخول</Text>
              </Text>
            </Button>
          </Content>
          {/* </KeyboardAvoidingView> */}
        </Container >
      );
    }
    return (
      <Container isRTL style={{ backgroundColor: '#E9EAEB' }} >
        {/* <KeyboardAvoidingView> */}
        {/* <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}> */}
        <Content padder style={styles.content} >
          <Text style={styles.titleStyle} >
            تسجيل حساب {this.props.types === 'Traders' ? 'تاجر' : typear}
          </Text>

          <Form style={styles.container}>
            <MyInput
              keyx='1' name={'user_phone'} im='phone'
              label='رقم الجوال' type='text'  update={this.update}
              value={this.state.user_phone}
              kp='numeric'
              type='phone'
            />

            <MyInput
              keyx='2' name={'password'} im='lock'
              label='كلمة المرور' secure type='text'
              update={this.update}
              value={this.state.password}              
            />

            <MyInput
              keyx='3' name={'name'} im='shop'
              label={'اسم ال' + typear} type='text'
              update={this.update}
              value={this.state.name}
            />

            <Item
              onPress={() => this.pickerShow()}
              rounded style={styles.item}
            >
              {
                this.state.imageok ?
                  (<Thumbnail square source={{ uri: this.state.image.uri }} />)
                  : (<View />)
              }
              <Input
                keyx='20' name={'image'} im='lock'
                disabled
                label='ارفاق الاستمارة'
                style={styles.inputStyle}
                placeholder={' صورة لل' + typear}
              />

              <Thumbnail
                square
                source={require('../assets/images/camera.png')}
                style={styles.iconImageBig}
              />
            </Item>

            <MyInput
              keyx='4' name={'Description'} im='note'
              label={'وصف ال' + typear} type='multi'
              update={this.update}
              value={this.state.Description}
            />


            <Item
              rounded style={styles.item}
              onPress={() => this.locationPicker()}
            >
              <Input
                keyx='21' name={'image'} im='lock'
                disabled
                label='ارفاق العنوان'
                style={styles.inputStyle}
                placeholder={'موقع ال' + typear}
              >
                <Text>{latitude ? `${latitude} ${longitude}` : ''}</Text>
              </Input>

              <Thumbnail
                square
                source={require('../assets/images/locat.png')}
                style={styles.iconImageBig}
              />
            </Item>

            {this.props.types === 'Traders' ?
              (
                <Item
                  rounded style={[styles.itemMulti, {}]}
                >
                  <View
                    style={{
                      flex: 9,
                      flexDirection: 'column',
                      borderRightWidth: 1,
                      borderColor: '#C4C4C4',
                    }}
                  >
                    <Input
                      keyx='21' name={'loc'} im='lock'
                      disabled
                      label='ارفاق الاستمارة'
                      style={[styles.inputStyle, { flex: 1, borderColor: '#fff', }]}
                      placeholder={'تخصيص ال' + typear}
                    />
                    <View style={{ flex: 1, flexDirection: 'row-reverse', }}>
                      <Radio
                        style={{ marginRight: 20, }}
                        onPress={() => this.setState({ AccountType: 'new' })}
                        selected={this.state.AccountType === 'new'}
                      />
                      <Text style={[styles.raidoStyle, {}]} >تاجر قطع غيار جديدة</Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row-reverse', }}>
                      <Radio
                        style={{ marginRight: 20, }}
                        onPress={() => this.setState({ AccountType: 'used' })}
                        selected={this.state.AccountType === 'used'}
                      />
                      <Text style={[styles.raidoStyle, {}]} >تاجر قطع غيار مستعملة</Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row-reverse', }}>
                      <Radio
                        style={{ marginRight: 20, }}
                        onPress={() => this.setState({ AccountType: 'motorcycle' })}
                        selected={this.state.AccountType === 'motorcycle'}
                      />
                      <Text style={[styles.raidoStyle, {}]} >تاجر دراجات نارية</Text>
                    </View>
                  </View>

                  <Thumbnail
                    square
                    source={require('../assets/images/locat.png')}
                    style={styles.iconImageBig}
                  />

                </Item>

              ) : (
                <View />
              )
            }

            <Text style={{ color: 'red', alignContent: 'center', flex: 1 }} >{this.props.user.errors.message}</Text>
            {this.renderButton()}
          </Form>
        </Content>
        {/* </KeyboardAvoidingView> */}
      </Container >
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.users
  };
}

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    submitin: adduserredux,
    resetLoging,
    myinforedux
  }, dispatch, getState);
};

//myInfo = connect(mapStateToProps, mapDispatchToProps, null, {withRef: true })(myInfo);
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(myInfo);
