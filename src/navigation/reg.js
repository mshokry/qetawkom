import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container, Content, Button, Text, ListItem,
  Form, Spinner, View, Thumbnail, Item, Input, Right, Radio
} from 'native-base';
// import { MyInput, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { compose } from 'recompose';
import { Formik } from 'formik';

import makeInputGreatAgain, {
  withNextInputAutoFocusForm,
  withNextInputAutoFocusInput,
} from 'react-native-formik';


import { adduserredux, resetLoging } from '../service/actions';
import styles from '../styles/forms';
import renderInput from '../Components/renderInputs';

const MyInput = compose(makeInputGreatAgain, withNextInputAutoFocusInput)(renderInput);
const Forms = withNextInputAutoFocusForm(Form);

export default props => (
  <Container>
    <Content>

      <Formik
        onSubmit={values => console.log(values)}
        // validationSchema={validationSchema}
        render={props => {
          return (

            <Forms
              style={styles.container}
            // onSubmit={(e) => { this.props.onSubmit }}
            >
              <MyInput key='3' name={'name'} im='lock' label='الاسم ' />
              <MyInput
                key='1' name={'user_phone'} im='phone'
                label='رقم الجوال'
                kp='numeric'
                type='phone'
              />
              <MyInput key='2' name={'password'} im='lock' label='كلمة المرور' secure />
              {/* <Text style={{ color: 'red', alignContent: 'center', flex: 1 }} >{this.props.user.errors.message}</Text> */}
              <Button
                Light rounded block style={styles.buttonStyle}
                onPress={props.handleSubmit} 
              // onPress={props.handleSubmit}
              >
                <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > تسجيل جديد</Text>
              </Button>
            </Forms>
          );
        }}
      />
    </Content>
  </Container>
);
