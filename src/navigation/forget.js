import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container, Content, Button, Text,
  Form, Spinner, View
} from 'native-base';
import { Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { resetLoging, loginuserredux } from '../service/actions';
import styles from '../styles/forms';
import renderInput from '../Components/renderInputs';

let MyInput;

class forget extends Component {
  constructor(props) {
    super(props);

    this.state = {
      typear: '',
      loaded: false,
    };
    MyInput = renderInput.bind(this);
  }

  componentWillMount() {
    console.log('Will Mount forget');
    if (this.props.user.logged === true) {
      console.log('forget User Has Token');
      Actions.auth({ type: 'reset' });
    }
  }

  componentDidMount() {
    console.log('Did Mount LogiAuth');
    this.props.resetLoging(null);
  }

  componentDidUpdate() {
    if (this.props.user.logged === true) {
      console.log('Logging User Has Token');
      Actions.auth();
    }
  }

  update = (prop, value) => {
    this.setState({
      [prop]: value
    });
  }

  renderButton() {
    if (this.props.user.logging !== undefined) {
      if (this.props.user.logging === true) {
        return (
          <Image
              source={require('../assets/images/loading.gif')}
              style={{ width: 50, height: 50, resizeMode: 'contain', marginTop: 5, alignSelf: 'center' }}
          />
        );
      }
    }
    return (
      <Button
        Light rounded block style={styles.buttonStyle}
        onPress={() => this.props.submitin(this.state)}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} >  تاكيد </Text>
      </Button>
    );
  }

  render() {
    return (
      <View >
          <Form style={styles.container}>
            <MyInput
              key='1' name={'user_phone'} im='phone'
              label='رمز التاكيد' 
              value={this.state.user_phone}
              update={this.update}
              type='phone'
              kp='numeric'
              req
            />
            <Text
              style={{ color: 'red', alignContent: 'center', flex: 1, textAlign: 'right' }}
            >{this.props.user.errors.message}</Text>
            {this.renderButton()}
          </Form>
          <Button
            transparent rounded blockٌ style={styles.buttonStyleTransperent}
            onPress={() => Actions.Loginauth()}
          >
          <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'black' }} > 
          اعادة ارسال رمز التحقق مرة اخري
          </Text>
          </Button>
          <Button
            transparent rounded blockٌ style={styles.buttonStyleTransperent}
            onPress={() => Actions.Loginauth()}
          >
          <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: 'black' }} > 
            تسجيل الدخول بحساب اخر
          </Text>
          </Button>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.users
  };
}

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    submitin: loginuserredux,
    resetLoging
  }, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(forget);
