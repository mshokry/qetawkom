import React, { Component } from 'react';
import { FlatList, LayoutAnimation, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
	Button, Text, View, Container, Content,
	Thumbnail, ListItem, Body, Badge
} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';

import styles from '../styles/forms';
import { LogoutToken, getRequestsRedux } from '../service/actions';

const { width, height } = Dimensions.get('window');

class Requests extends Component {
	constructor(props) {
		super(props);
		this.state = {
			refreshing: false,
		};
	}

	componentWillMount() {
		if (this.props.users.token === null) {
			console.log('Requests User not logged');
			Actions.Reg({ type: 'reset' });
		} else {
			console.log('Requests Component Updates PARTS !!', this.props.parts.requests);
			//if (!(Object.keys(this.props.parts.requests).length > 0)) {
				const { token } = this.props.users;
				const { types } = this.props;
				this.props.getRequests(token, 0, types);
			//}
		}
	}

	componentWillUpdate() {
		LayoutAnimation.spring();
	}

	render() {
		const { token } = this.props.users;
		const { types } = this.props;
		const count = Object.keys(this.props.parts.requests).length;
		console.log('Total Requests', count);
		//console.log('Total Settings', Object.keys(this.props.parts.settings).length);
		console.log('## Props Reuest', this.props);

		return (
			<Container
				style={{ flex: 1, backgroundColor: '#E9EAEB', width }}
				contentContainerStyle={{ flexGrow: 1 }}
			>
				{/* <HeaderMain title="##قطعكم##" /> */}
				<View style={{ flex: 1, backgroundColor: '#E9EAEB', width, marginLeft: -10, }} >
					{/* <Content padder style={[styles.content, { width: '100%', }]} > */}
					{/* {this.renderLoading()} */}
					{!(count > 0) ?
						(<View >
							<Spinner
								visible={this.props.parts.loading === null ? false : this.props.parts.loading}
								textContent={'جاري التحميل ....'} textStyle={{ color: '#0071D0' }}
							/>
						{this.props.parts.loading ?
						(<View />)
						:
						(<Text style={[styles.baseText, { alignSelf: 'flex-end' }]} >
								لا توجد بيانات
							</Text>
						)	
					}						
						</View>) :
						(
							<View style={{ flex: 1, }}>
								<Text style={styles.titleStyle} > الطلبات </Text>
								<FlatList
									// data={[{ name: 'Title Text', add_date: 'item1', ID: 14, Offers: 1 }]}
									style={{ width, marginTop: 20, }}
									data={this.props.parts.requests}
									refreshing={this.props.parts.loading}
									onRefresh={() =>
										this.props.getRequests(token, 0, types)
									}
									onEndReachedThreshold={0.85}
									onEndReached={() =>
										this.props.getRequests(token, count, types)
									}
									renderItem={({ item, index }) => {
										return (
											<ListItem
												key={item.ID} onPress={() => Actions.Part({ item })}
												style={{ backgroundColor: index % 2 ? '#E9EAEB' : 'lightgray', flex: 1, }}
											>
												<Body >
													<Text style={styles.baseText}>{item.name ? item.name : 'بدون عنوان'} </Text>
													<View style={{ flexDirection: 'row-reverse', }} >
														<View style={{ flex: 9 }}>
															<Text note style={styles.noteText} > تاريخ الاعلان {item.add_date}</Text>
															<Text note style={styles.noteText} > رقم الاعلان {item.ID}</Text>
														</View>
														{item.OffersCount ?
															(<Badge
																style={{ flex: 2, backgroundColor: '#A0A0A0', height: 40, alignContent: 'center', marginLeft: 20, }}
															>
																<Text style={[styles.baseText, { marginTop: 12, }]}>
																	{item.OffersCount} عرض</Text>
															</Badge>)
															:
															(<View />)
														}
													</View>
												</Body>
											</ListItem>

										);
									}
										// </TouchableWithoutFeedback>
									}
									keyExtractor={(item, index) => item.ID.toString()}
								/>
							</View>
						)
					}
					{/* </Content> */}
				</View>

			</Container >
		);
	}
}

const mapStateToProps = (state) => ({
	users: state.users,
	parts: state.parts,
});

const mapDispatchToProps = (dispatch, getState) => {
	return bindActionCreators({
		LogoutToken,
		getRequests: getRequestsRedux
	}, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Requests);

