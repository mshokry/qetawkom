import React, { Component } from 'react';
import { Image } from 'react-native';
import { List, ListItem, Text, Body, } from 'native-base';
import styles from './style';

class Offer extends Component {
    render() {
        return (
            <List>
                <ListItem>
                    <Body>
                        <Text style={styles.baseText}>طلب عميل </Text>
                        <Text note style={styles.noteTextRed} >تاريخ 5/55/55555</Text>
                        <Text note style={styles.noteText} > رقم : 5435353534 </Text>
                    </Body>
                    <Image style={styles.ImageStyle} source={require('../images/car1.jpg')} />
                </ListItem>
                <ListItem>
                    <Body >
                        <Text style={styles.baseText}>طلب عميل </Text>
                        <Text note style={styles.noteTextRed} >تاريخ 5/55/55555</Text>
                        <Text note style={styles.noteText} > رقم : 5435353534 </Text>
                    </Body>
                    <Image style={styles.ImageStyle} source={require('../images/car2.png')} />
                </ListItem>
                <ListItem>
                    <Body>
                        <Text style={styles.baseText}>طلب عميل </Text>
                        <Text note style={styles.noteTextRed} >تاريخ 5/55/55555</Text>
                        <Text note style={styles.noteText} > رقم : 5435353534 </Text>
                    </Body>
                    <Image style={styles.ImageStyle} source={require('../images/car3.jpg')} />
                </ListItem>
            </List>
        );
    }
}

export default Offer;
