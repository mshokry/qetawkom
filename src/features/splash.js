import React, { Component } from 'react';
import {
	Image, Dimensions, TouchableOpacity,
	AsyncStorage
} from 'react-native';
import { bindActionCreators } from 'redux';
import { Text, View, Container, Content } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import styles from '../styles/forms';
import { retrieveData, loginuserredux } from '../service/actions';

const { width, height } = Dimensions.get('window');

class Splash extends Component {

	async componentWillMount() {
		//	if (process.env.NODE_ENV !== 'development') {
		console.log('Will mount Splash Checking Storage');
		try {
			const value = await AsyncStorage.getItem('token');
			if (value !== null) { // We have data!!
				console.log('Storage conatins Token', value);
				this.props.loginuser(JSON.parse(value)); // Redux Login
			}//Goto main with interval
		} catch (error) {
			console.log('error loading data', error);
			Actions.MainNotlog({ type: 'reset' });
		}
		//}
		this.intervalId = setInterval(() => {
			console.log('Interval LogiAuth');
			clearInterval(this.intervalId);
			Actions.MainNotlog();
		}, 10000);
		console.log('### Storage Main OR Splash ###');
	}

	componentDidUpdate() {
		// console.log('did mount Splash', this.props.users);
		if (this.props.users.logged === true) {
			clearInterval(this.intervalId);
			console.log('Logging User Has Token');
			if (this.props.users.token.active === 'no') {
				Actions.activate({ type: 'reset' });
			} else {
				Actions.auth({ type: 'reset' });
			}
		}
		if (this.props.users.splash === true) {
			console.log('Splash Error');
			Actions.MainNotlog({ type: 'reset' });
		}
	}

	render() {
		return (
			<Container isRTL >
				{/* <HeaderMain title="##قطعكم##" /> */}
				<Content padder style={[styles.Content, { backgroundColor: '#0071D0' }]} >
					<TouchableOpacity style={{ flex: 1, }} >
						<View
							style={[styles.Container, {
								flexDirection: 'column',
								paddingTop: (height / 2) - 180,
							}]}
						>
							<Image
								source={require('../assets/images/blogo.png')}
								style={{
									height: 120,
									width: 120,
									resizeMode: 'stretch',
									tintColor: '#FFF',
									marginTop: 5,
									alignSelf: 'center'
								}}
							/>
							<Text
								style={[styles.titleStyle, { alignSelf: 'center', color: '#FFF', borderColor: '#0071D0', }]}
							>
								بيع .. شراء .. استيراد</Text>
						</View>
					</TouchableOpacity>
				</Content>
			</Container>
		);
	}
}

const mapStateToProps = (state) => ({
	users: state.users
});

const mapDispatchToProps = (dispatch, getState) => {
	return bindActionCreators({
		loginuser: loginuserredux,
		retrieveData
	}, dispatch, getState);
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Splash);
