import React, { Component } from 'react';
import { Modal } from 'react-native';
import { Container, Header, Left, Button, Icon, Title, Body, Right, Content, Text } from 'native-base';


class Modals extends Component {
	state = {
		modalVisible: false,
	};

	_setModalVisible(visible) {
		this.setState({ modalVisible: visible });
	}
	render() {
		return (
			<Modal
				animationType={"slide"}
				transparent={false}
				visible={this.state.modalVisible}
				onRequestClose={() => this._setModalVisible(false)}
			>
				<Container>
					<Header>
						<Left>
							<Button transparent onPress={this._toggleRequestModal.bind(this, false)} >
								<Icon name='ios-arrow-down' />
							</Button>
						</Left>
						<Body>
							<Title>{this.props.title}</Title>
						</Body>
						<Right />
					</Header>

					<Content>
						<Button
							onPress={this._toggleRequestModal.bind(this, false)}
						>
							<Text>Close</Text>
						</Button>
					</Content>
				</Container>
			</Modal>
		);
	}
}

export default Modals;
