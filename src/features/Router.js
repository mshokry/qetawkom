import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
// import { Scene, Router, Actions } from 'react-native-router-flux';
import HeaderMain from './HeaderMain';
import Loginauth from './Loginauth';
import ToRegister from './ToRegister';
import Register from './Register';
import Requests from './Requests';
// import Request from './Requests';
import Offer from './Offer';
import Home from './Home';

class Routers extends Component {
  render() {
    console.log(this.props.navigation);
    return (
      <RootStack />
    );
  }
}
//   <Text style={styles.titleStyle} > تسجيل حساب جديد </Text>

export default Routers;
const RootStack = createStackNavigator(
  {
    ScreenRequests: { screen: Requests },
    // Request: { screen: Request},
    ScreenOffer: { screen: Offer, Header: HeaderMain },
    ToRegisterS: {
      screen: ToRegister,
      navigationOptions: ({ navigation }) => ({
        header: HeaderMain,
      })
    },
    ScreenRegister: { screen: Register },
    ScreenLoginauth: { screen: Loginauth },
    main: {
      screen: Home,
      navigationOptions: ({ navigation }) => ({
        header: HeaderMain,
      }),
    },
  },
  {
    initialRouteName: 'ToRegisterS',
    // headerMode: 'none',
  }
);
