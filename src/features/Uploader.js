import { create } from 'apisauce';

// create api. 
const api = create({
    baseURL: 'http://localhost:3000',
});

// create formdata
const data = new FormData();
data.append('name', 'testName');
image.forEach((photo, index) => {
    data.append('photos', {
        uri: photo.uri,
        type: 'image/jpeg',
        name: 'image' + index
    });
});

// post your data.
api.post('/array', data, {
    onUploadProgress: (e) => {
        console.log(e)
        const progress = e.loaded / e.total;
        console.log(progress);
        this.setState({
            progress: progress
        });
    }
})
    .then((res) => console.log(res));

// if you want to add DonwloadProgress, use onDownloadProgress
onDownloadProgress: (e) => {
    const progress = e.loaded / e.total;
}
