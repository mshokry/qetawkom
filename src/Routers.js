import React from 'react';
import { Scene, Router, Stack } from 'react-native-router-flux';
import HeaderMain from './Components/HeaderMain';
import HeaderLogin from './Components/HeaderLogin';
// import Loginauth from './Components/Loginauth';
import ToRegister from './navigation/ToRegister';
import Register from './navigation/Register';
import MainNotlog from './navigation/MainNotlog';
import Loginauth from './navigation/Loginauth';
import activate from './navigation/activate';
import myInfo from './navigation/myInfo';

import Main from './navigation/Main';
import partRequest from './navigation/partRequest';
import Import from './navigation/import';
import Splash from './features/splash';
import Requests from './navigation/Requests';
import RequestDetail from './navigation/RequestDetail';
import OfferDeatail from './navigation/OfferDeatail';

const RouterComponent = () => {
  console.log('Main Screen 2 ');

  return (
    <Router sceneStyle={{ paddingTop: 0 }} >
      <Stack key="root">

        {/* <Scene key="Reg" hideNavBar > */}
        <Scene key="Splash" component={Splash} hideNavBar initial />
        <Scene key="Reg" component={Splash} hideNavBar initial />
          <Scene key="MainNotlog" component={MainNotlog} navBar={HeaderLogin} hideNavBar={false} />
          <Scene key="ToRegister" component={ToRegister} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="Register" component={Register} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="Loginauth" component={Loginauth} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="activate" component={activate} navBar={HeaderMain} hideNavBar={false} />
        {/* </Scene> */}
       
        {/* <Scene key="auth" hideNavBar > */}
          <Scene key="auth" component={Main} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="Main" component={Main} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="partRequest" component={partRequest} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="Import" component={Import} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="Requests" component={Requests} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="Part" component={RequestDetail} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="Offer" component={OfferDeatail} navBar={HeaderMain} hideNavBar={false} />
          <Scene key="myInfo" component={myInfo} navBar={HeaderMain} hideNavBar={false} />
          {/* <Scene key="Request" component={Request} navBar={HeaderMain} hideNavBar={false} /> */}
        {/* </Scene> */}
      
      </Stack>
    </Router>
  );
};

export default RouterComponent;
