import { Alert } from 'react-native';
import { create } from 'apisauce';
import { Actions } from 'react-native-router-flux';
import qs from 'qs';

const api = create({
	//    baseURL: 'https://msn-hr-system.herokuapp.com/api/v1',
	baseURL: 'http://qeta3km.a5aa.com/',
	// baseURL: "https://httpbin.org",

	headers: {
		'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
	},
	timeout: 30000
});

/**********************************************************************
 * Genera Functons
 ***********************************************************************/
export const loading = (values) => ({
	type: 'loading',
	payload: values
});

export const resetLoading = (values) => ({
	type: 'unloading',
	payload: values
});

export const AlertOK = (values) => ({
	type: 'alertOK',
	payload: values
});

export const resetAlert = (values) => ({
	type: 'unAlert',
	payload: values
});

export const partstError = (values) => ({
	type: 'Part Error',
	payload: values
});

export const progress = (values) => ({
	type: 'progress',
	payload: values
});

/**********************************************************************
 * Add Part Functions
 ***********************************************************************/

export const addpart = (values) => ({
	type: 'Add Part',
	payload: values
});

export const addPartRedux = (data) => {
	return (dispatch, getState) => {
		//dispatch(loading());
		console.log('addPArtREdux', data);
		const values = {
			json_email: data.token.username,
			json_password: data.token.password,
			name: data.text ? data.text : '',
			car: data.selected.ID,
			model: data.selectedSub.ID,
			make_year: data.selectedYear,
			type: data.type,
			ImportCountry: data.import_image ? data.import_image : '',
			FormNumber: data.number ? data.number : '',
			do: 'insert'
		};
		const val = new FormData();
		//val.append(values);

		Object.keys(values).map((key) =>
			val.append(key, values[key])
		);
		if (data.imageok) {
			val.append('image', {
				uri: data.image.uri,
				type: data.image.type,
				name: data.image.name
			});
		}
		//dispatch(AlertOK(null));
		dispatch(loading());
		console.log('Response form addPartRedux  Valuse', qs.stringify(values, { encode: true }));
		console.log('Response form addPartRedux  Val', val);
		api
			.setHeader('Content-Type', 'multipart/form-data;charset=UTF-8');
		api
			.post('/partRequest-edit-1.html?json=true&ajax_page=true&app=IOS',
				//qs.stringify(val, { encode: true }),
				val,
				{
					onUploadProgress: (e) => {
						console.log(e);
						const prog = e.loaded / e.total;
						console.log(prog);
						dispatch(progress(prog));
					}
				})
			.then((r) => {
				console.log('Response form addPartRedux', r.data);
				if (r.ok === true) {
					const setting = qs.parse(r.data);
					dispatch(addpart(setting));
				} else {
					dispatch(resetLoading());
					dispatch(partstError('حدث خطأ برجاء اعادة المحاولة'));
				}
			})
			.catch(
				(e) => {
					console.log('submitting form Error ', e);
					dispatch(resetLoading());
					dispatch(partstError('حدث خطأ برجاء اعادة المحاولة'));
					dispatch(partstError('حدث خطأ برجاء اعادة المحاولة'));
				}
			);
	};
};

/**********************************************************************
 * Add Offer Functions
 ***********************************************************************/

export const addoffer = (values) => ({
	type: 'Add Offer',
	payload: values
});

//- [ ] params.put("DeliveryLocation", Location);
//params.put("Request", RequestID);

export const addOfferRedux = (data) => {
	return (dispatch, getState) => {
		//dispatch(loading());
		console.log('addOFFERREdux', data);
		const values = {
			json_email: data.token.username,
			json_password: data.token.password,
			Price: data.Price ? data.Price : '',
			Specifications: data.Specifications ? data.Specifications : '',
			DeliveryLocation: data.DeliveryLocation ? data.DeliveryLocation : '',
			DeliveryTime: data.DeliveryTime ? data.DeliveryTime : '',
			Request: data.ID,
			do: 'insert'
		};
		let val = new FormData();
		//val.append(values);

		Object.keys(values).map((key) =>
			val.append(key, values[key])
		);
		if (data.imageok === true) {
			console.log('########Image########');
			val.append('image', {
				uri: data.image.uri,
				type: data.image.type,
				name: data.image.fileName,
			});
			values.image = {
				uri: data.image.uri,
				type: data.image.type,
				name: data.image.fileName,
			};
			//api.setHeaders({ 'Content-Type': 'Content-Type: multipart/form-data;', });
			//val = qs.stringify(values, { encode: true });
		} else {
			api.setHeaders({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
			val = qs.stringify(values, { encode: true });
		}
		//dispatch(AlertOK(null));
		dispatch(loading());
		console.log('Response form addPartRedux  Valuse', qs.stringify(values, { encode: true }));
		console.log('Response form addPartRedux  Val', val);

		api
			.post('/RequestOffers-edit-1.html?json=true&ajax_page=true&app=IOS',
				//qs.stringify(val, { encode: true }),
				val,
				{
					onUploadProgress: (e) => {
						console.log(e);
						const prog = e.loaded / e.total;
						console.log(prog);
						dispatch(progress(prog));
					}
				})
			.then((r) => {
				console.log('Response form addPartRedux', r.data);
				if (r.data.ID === undefined) {
					dispatch(resetLoading());
					dispatch(partstError('حدث خطأ برجاء اعادة المحاولة'));
				} else {
					const offer = qs.parse(r.data);
					dispatch(addoffer(offer));
				}
			})
			.catch(
				(e) => {
					console.log('submitting form Error ', e);
					dispatch(resetLoading());
					dispatch(partstError('حدث خطأ برجاء اعادة المحاولة'));
				}
			);
	};
};


/**********************************************************************
 * Get Orders Functions
 ***********************************************************************/

export const getRequests = (settings) => ({
	type: 'Get Requests',
	payload: settings
});

export const getRequestsRedux = (token, refresh, types) => {
	return (dispatch, getState) => {
		dispatch(loading());
		//const values = `json_email=${token.username}&json_password=${token.password}&do=GetSetting`;
		const values = {
			json_email: token.username,
			json_password: token.password,
			Type: types ? types : 'new',
			do: 'Requests'
		};

		if ((refresh === getState().parts.refresh && refresh !== 0) || refresh === undefined) {
			dispatch(resetLoading());
		} else {
			console.log('Response form  Valuse', qs.stringify(values, { encode: true }));
			api
				.post(`/include/webService.php?json=true&app=IOS&start=${refresh}&end=15`,
					qs.stringify(values, { encode: true }))
				.then((r) => {
					if (r.ok === true) {
						console.log('Response form getRequest', r.data);
						const requests = r.data;
						dispatch({ type: 'Refresh', payload: refresh });
						dispatch(getRequests(requests));
					} else {
						console.log('Response form getRequest Error', r.data);
						dispatch(resetLoading());
					}
				})
				.catch(
					(e) => {
						console.log('submitting form Error ', e);
						dispatch(getRequestsRedux(token));
					}
				);
		}
	};
};


/**********************************************************************
 * Get Settings Functions
 ***********************************************************************/

export const getSettings = (settings) => ({
	type: 'Get Settings',
	payload: settings
});

export const getSettingRedux = (token) => {
	return (dispatch, getState) => {
		dispatch(loading());
		//const values = `json_email=${token.username}&json_password=${token.password}&do=GetSetting`;
		const values = {
			json_email: token.username,
			json_password: token.password,
			do: 'GetSetting'
		};
		console.log('Response form  Valuse', qs.stringify(values, { encode: true }));
		if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
			// dev code
			const data = {
				"Cars": [
					{
						"ID": 2, "name": "تويوتا", "type": "1", "photo": "larg1529434846.png", "Models": [
							{ "ID": 3, "name": "كورولا", "type": "1" }]
					},
					{
						"ID": 6, "name": "سوزوكى", "type": "3", "photo": "larg1529434996.png", "Models": [
							{ "ID": 6, "name": "بلوفورد", "type": "2" },
							{ "ID": 4, "name": "سويفت", "type": "1" },
							{ "ID": 5, "name": "كوبرا 400", "type": "2" }]
					},
					{
						"ID": 7, "name": "كاوازاكى", "type": "2", "photo": "larg1529435070.png", "Models": [
							{ "ID": 7, "name": "Ninja 250R", "type": "2" }]
					},
					{
						"ID": 1, "name": "كيا", "type": "1", "photo": "larg1529434968.png", "Models": [
							{ "ID": 1, "name": "كارينز", "type": "1" }]
					},
					{
						"ID": 3, "name": "مرسيدس", "type": "1", "photo": "larg1529434808.png", "Models": [
							{ "ID": 2, "name": " c36", "type": "1" }]
					}],
				"Country": [
					{ "ID": 2, "name": "الامارات" },
					{ "ID": 3, "name": "البحرين" },
					{ "ID": 4, "name": "السعوديه" },
					{ "ID": 7, "name": "الكويت" },
					{ "ID": 6, "name": "عمان" },
					{ "ID": 5, "name": "قطر" },
					{ "ID": 1, "name": "مصر" }
				]
			};
			const setting = qs.parse(data);
			dispatch(getSettings(setting));
		} else {
			api
				.post('include/webService.php?json=true&app=IOS', qs.stringify(values, { encode: true }))
				.then((r) => {
					console.log('Response form getsettings', r.data);
					if (r.ok === true) {
						const setting = qs.parse(r.data);
						if (setting === "Invalid login") {
							dispatch('logout');
						} else {
							dispatch(getSettings(setting));
						}
					} else {
						dispatch(resetLoading());
						//dispatch(loginusererror(r));
						//throw new SubmissionError({ error: { 'email': 'ERROR' } });
					}
				})
				.catch(
					(e) => {
						console.log('submitting form Error ', e);
						dispatch(getSettingRedux(token));
					}
				);
		}
	};
};
