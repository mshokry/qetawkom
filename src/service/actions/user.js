import { AsyncStorage, NativeModules } from 'react-native';
import { create } from 'apisauce';
import { Actions } from 'react-native-router-flux';
//import RNFS from 'react-native-fs';
import qs from 'qs';

//const RNFS = require('react-native-fs');

/*
- [ ] #TODO Handle User Cancel or click back
*/
const url = 'http://qeta3km.a5aa.com/';
const api = create({
	baseURL: url,
	timeout: 30000
});

const storeData = async (data) => {
	try {
		await AsyncStorage.setItem('token', JSON.stringify(data));
		console.log('storeData saved');
	} catch (error) {
		// Error saving data
		console.log('storeData Not saved');
	}
};

export const retrieveData = async () => {
	try {
		const value = await AsyncStorage.getItem('token');
		if (value !== null) {
			console.log('Storage conatins Token', value);
			loginuser(value);
			//Actions.auth({ type: 'reset' });
		}
	} catch (error) {
		// Error retrieving data
		console.log('error loading data');
	}
};

export const resetLoging = (values) => ({
	type: 'unlogging',
	payload: values
});

export const adduser = (values) => ({
	type: 'Add User',
	payload: values
});

export const progressUser = (values) => ({
	type: 'progress user',
	payload: values
});

//May check prev coommit for another way
export const adduserredux = (data) => {
	return (dispatch, getState) => {
		dispatch({ type: 'logging' });
		const values = {};
		// const { image, loc, imageok, type } = getState();
		console.log("######Data###", data);
		values.username = `00966${data.user_phone}`;
		if (data.Description) { values.Description = data.Description; }
		if (data.AccountType) { values.AccountType = data.AccountType; }
		if (data.loc !== undefined) {
			if (data.loc.latitude !== undefined) {
				values.location = `${data.loc.latitude}||${data.loc.longitude}||${data.loc.address}`;
			}
		}
		values.name = data.name;
		values.password = data.password;
		values.do = 'insert';
		let val = new FormData();
		//val.append(values);
		Object.keys(values).map((key) =>
			val.append(key, values[key])
		);
		if (data.imageok === true) {
			console.log('########Image########');
			val.append('image', {
				uri: data.image.uri,
				type: data.image.type,
				name: data.image.fileName,
			});
			values.image = {
				uri: data.image.uri,
				type: data.image.type,
				name: data.image.fileName,
			};
			//api.setHeaders({ 'Content-Type': 'Content-Type: multipart/form-data;', });
			//val = qs.stringify(values, { encode: true });
		} else {
			api.setHeaders({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
			val = qs.stringify(values, { encode: true });
		}

		console.log('Response form  Valuse', qs.stringify(values, { encode: true }));
		console.log('Response form  Valuse', val);
		let apiLink;
		if (data.type === 'Traders') {
			apiLink = '/trader-edit-1.html?json=true&ajax_page=true&app=IOS';
		} else if (data.type === 'Workshop') {
			apiLink = '/workshop-edit-1.html?json=true&ajax_page=true&app=IOS';
		} else {
			apiLink = '/users-edit-1.html?json=true&ajax_page=true&app=IOS';
		}

		api
			.post(apiLink,
			// .post('https://httpbin.org/post',
				val,
				{
					onUploadProgress: (e) => {
						console.log(e);
						const prog = e.loaded / e.total;
						console.log(prog);
						dispatch(progressUser(prog));
					}
				})
			.then((r) => {
				console.log('Response form adduser', r.data);
				if (r.ok === true) {
					// Save Current token
					const token = qs.parse(r.data);
					if (token.ID === undefined) {
						if (r.data[0] === 'Field password  From 6 To 20') {
							dispatch(loginusererror('كلمة المرور بين 6 و 20'));
						} else if (r.data[0] === 'Field username  Already exist') {
							dispatch(loginusererror('المستخدم موجود بالفعل'));
						} else {
							dispatch(loginusererror('حدث خطأ برجاء اعادة المحاولة'));
						}
					} else {
						console.log('saving in login');
						token.passwordP = data.password;
						storeData(token);
						dispatch(loginuser(token));
					}
				} else {
					dispatch(loginusererror('حدث خطأ برجاء اعادة المحاولة'));
				}
			})
			.catch((e) => {
				dispatch(loginusererror('حدث خطأ برجاء اعادة المحاولة'));
				console.log('submitting form Error ', e);
			});
	};
};


export const LogoutToken = (token) => {
	return dispatch => {
		dispatch({ type: 'Logout User Token', payload: token });
		dispatch({ type: 'clearSettings', payload: token });
	};
};

export const ErrorUser = (token) => ({
	type: 'Login Error',
	payload: token
});

export const loginusererror = (error: string) => ({
	type: 'Login Error',
	payload: error
});

export const loginuser = (token) => ({
	type: 'Login User',
	payload: token
});

export const loginuserredux = (data) => {
	return dispatch => {
		dispatch({ type: 'logging' });
		const values = {
			email: data.user_phone ? `00966${data.user_phone}` : data.username,
			do: 'log',
			password: data.passwordP
		};
		console.log('Response form  Valuse', qs.stringify(values, { encode: true }));
		api.setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		api
			.post('/include/pages/login_cust.php?json=true&app=IOS', qs.stringify(values, { encode: true }))
			.then((r) => {
				console.log('Response form Login', r);
				if (r.ok === true) { // Save Current token
					const token = qs.parse(r.data);
					if (token.ID === undefined) {
						console.log('Error in login ####');
						const error = 'حدث خطأ برجاء اعادة المحاولة';
						dispatch(ErrorUser(error));
					} else { //Saving in storage
						console.log('saving in login');
						token.passwordP = values.password;
						token.user_phone = values.user_phone;
						storeData(token);
						dispatch(loginuser(token));
					}
				} else {
					console.log('Error in login XX');
					dispatch(ErrorUser('حدث خطأ برجاء اعادة المحاولة'));
				}
			})
			.catch((e) => {
				dispatch(ErrorUser('حدث خطأ برجاء اعادة المحاولة'));
				console.log('submitting form Error ', e);
			});
	};
};

/*
- [ ] My info
#ToDO {"json_password":"e1ddd2997d6f3dc1dce755848d2636893e","json_email":"00966123456123","do":"MyInfo"}
*/
export const myInfo = (data: string) => ({
	type: 'my info',
	payload: data
});

export const myinforedux = (data) => {
	return dispatch => {
		dispatch({ type: 'logging' });
		const values = {
			json_email: data.user_phone ? `00966${data.user_phone}` : data.username,
			json_password: data.password,
			do: 'MyInfo',
		};
		console.log('Response form  Valuse', qs.stringify(values, { encode: true }));
		api.setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		api
			.post('/include/webService.php?json=true&app=IOS', qs.stringify(values, { encode: true }))
			.then((r) => {
				console.log('Response form ', r);
				if (r.ok === true) { // Save Current token
					const token = qs.parse(r.data);
					if (token.ID === undefined) {
						const error = 'حدث خطأ برجاء اعادة المحاولة';
						dispatch(ErrorUser(error));
					} else {
						
					}
				} else {
					dispatch(ErrorUser('حدث خطأ برجاء اعادة المحاولة'));
				}
			})
			.catch((e) => console.log('submitting form Error ', e));
	};
};

export const forget = (data: string) => ({
	type: 'Forget',
	payload: data
});

export const forgetredux = (data) => {
	return dispatch => {
		dispatch({ type: 'sending' });
		const values = {
			email: data.user_phone ? `00966${data.user_phone}` : data.username,
			AppID: '54344776543345',
			do: 'sendPassword',
		};
		console.log('Response form  Valuse', qs.stringify(values, { encode: true }));
		api.setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		api
			.post('/include/webService.php?json=true&app=IOS&do=sendPassword', qs.stringify(values, { encode: true }))
			.then((r) => {
				console.log('Response form ', r);
				if (r.ok === true) { // Save Current token
					const token = qs.parse(r.data);
					if (token.ID === undefined) {
						const error = 'حدث خطأ برجاء اعادة المحاولة';
						dispatch(ErrorUser(error));
					} else {
						dispatch(forget(null));
					}
				} else {
					dispatch(ErrorUser('حدث خطأ برجاء اعادة المحاولة'));
				}
			})
			.catch((e) => console.log('submitting form Error ', e));
	};
};

export const active = (data: string) => ({
	type: 'Forget',
	payload: data
});

export const activeredux = (data) => {
	return dispatch => {
		dispatch({ type: 'logging' });
		const values = {
			email: data.token.user_phone ? `00966${data.token.user_phone}` : data.token.username,
			password: data.token.password,
			activation_code: data.code,
			do: 'activation_code',
		};
		console.log('Response form  Valuse Activation', qs.stringify(values, { encode: true }));
		api.setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		api
			.post('/include/webService.php?json=true&app=IOS', qs.stringify(values, { encode: true }))
			.then((r) => {
				console.log('Response form ', r);
				if (r.ok === true) { // Save Current token
					const token = qs.parse(r.data);
					if (token.Result === 'Success' || token.Result === '-1') {
						dispatch(loginuserredux(data.token));
					} else {
						const error = 'حدث خطأ برجاء اعادة المحاولة';
						dispatch(ErrorUser(error));
					}
				} else {
					dispatch(ErrorUser('حدث خطأ برجاء اعادة المحاولة'));
				}
			})
			.catch((e) => console.log('submitting form Error ', e));
	};
};

export const resendredux = (data) => {
	return dispatch => {
		dispatch({ type: 'sending' });
		const values = {
			email: data.user_phone ? `00966${data.user_phone}` : data.username,
			password: data.password,
			do: 'reactivation_code',
		};
		console.log('Response form  Valuse', qs.stringify(values, { encode: true }));
		api.setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		api
			.post('include/webService.php?json=true&app=IOS', qs.stringify(values, { encode: true }))
			.then((r) => {
				console.log('Response form ', r);
				if (r.ok === true) { // Save Current token
					const token = qs.parse(r.data);
					//if (token.ID === undefined) {
						const error = 'تم ارسال الكود';
						dispatch(ErrorUser(error));
					//} else {

					//}
				} else {
					dispatch(ErrorUser('حدث خطأ برجاء اعادة المحاولة'));
				}
			})
			.catch((e) => console.log('submitting form Error ', e));
	};
};
