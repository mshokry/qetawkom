import { create } from 'apisauce';
import { PHONE_CHANGE, PASSWORD_CHANGE, USER_LOGING_OK, USER_LOGING_ERROR } from './types';

export const selectLibrary = (libraryId) => {
    return {
        type: 'select_library',
        payload: libraryId
    };
};

export const passwordChanged = (password) => ({
    type: PASSWORD_CHANGE,
    payload: password
});

export const phoneChanged = (phone) => ({
    type: PHONE_CHANGE,
    payload: phone
});

export const loginUser = ({ phone, password }) => {
    return (dispatch) => {
        console.log('dd');
        const api = create({
            baseURL: 'https://api.github.com',
            headers: { 'Accept': 'application/vnd.github.v3+json' }
        });
        api.post('/users', { name: phone }, { headers: { 'x-gigawatts': '1.21' } })
            .then((response) => {
                dispatch({ type: USER_LOGING_OK, payload: response });
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: USER_LOGING_ERROR, payload: error });                
            });
    };
};
