import React, { Component } from 'react';

import {
	Item, Input, Thumbnail, Textarea, Picker,
	Text,
	Label
} from 'native-base';
import styles from '../styles/forms';


export default renderInput = (props) => {
	const {
		input, label, type, key, secure,
		im, req, kp, disabled,
		inputProps,
		meta: { visited, error, warning }
	} = props;
	console.log('Props :', props);

	let image;
	switch (im) {
		case 'lock': image = require('../assets/images/lock.png'); break;
		case 'locat': image = require('../assets/images/locat.png'); break;
		case 'edit': image = require('../assets/images/edit.png'); break;
		case 'profile': image = require('../assets/images/profile.png'); break;
		case 'mech': image = require('../assets/images/mech.png'); break;
		case 'clint': image = require('../assets/images/clint.png'); break;
		case 'shop': image = require('../assets/images/shop.png'); break;
		case 'camera': image = require('../assets/images/camera.png'); break;
		case 'phone': image = require('../assets/images/phone.png'); break;
		case 'note': image = require('../assets/images/note.png'); break;
		default:
			break;
	}

	let hasError = false;
	if (error !== undefined && visited) {
		hasError = true;
	}
	if ((input.value === undefined || input.value === '') && visited && req) {
		hasError = true;
	}
	if (type === 'multi') {
		return (
			<Item error={hasError} key={key} rounded style={styles.itemMulti}>
				<Textarea
					style={styles.inputStyle}
					{...input} key="1{key}"
					placeholder={label}
					rowSpan={5}
				/>
				<Thumbnail
					square
					source={image}
					style={styles.iconImageBig}
				/>
			</Item>
		);
	}
	if (type === 'picker') {
		const { input: { onChange, value, ...inputProps }, children, ...pickerProps } = props;
		return (
			<Picker
				style={styles.item}
				selectedValue={value}
				onValueChange={value => onChange(value)}
				{...inputProps}
				{...pickerProps}
			>
				{children}
			</Picker>
		);
	}
	if (type === 'phone') {
		return (
			<Item error={hasError} key={key} rounded style={styles.item}>
				{/* <Label>{label}    <Text style={{ color: 'red' }}>    {error}</Text></Label> */}
				<Text
					style={{ fontFamily: 'Cairo-Light', fontSize: 20, marginLeft: 10, }}
				>00966</Text>
				<Input
					// {...input} 
					{...inputProps}
					key="1{key}"
					style={styles.inputStyle}
					secureTextEntry={secure}
					keyboardType={kp ? kp : 'default'}
					placeholder={label}
					keyboardShouldPersistTaps='always'
					onChangeText={input.onChange}
					onBlur={input.onBlur}
					onFocus={input.onFocus}
					value={input.value}
				/>
				<Thumbnail
					square
					source={image}
					style={styles.iconImageBig}
				/>
				{/* {hasError ? <Icon name='close-circle' /> : <Icon />} */}
			</Item >
		);
	}	
	return (
		<Item error={hasError} key={key} rounded style={styles.item}>
			{
				disabled === true ? (
					<Label
						{...input} key="1{key}"
						style={[styles.inputStyle, { backgroundColor: '#000', }]}
					>label</Label>
				) :
					(
						<Input
							{...input} key="1{key}"
							style={styles.inputStyle}
							secureTextEntry={secure}
							keyboardType={kp ? kp : 'default'}
							placeholder={label}
							onChangeText={input.onChange}
							onBlur={input.onBlur}
							onFocus={input.onFocus}
							value={input.value}
						/>
					)
			}
			<Thumbnail
				square
				source={image}
				style={styles.iconImageBig}
			/>
		</Item>
	);
};

