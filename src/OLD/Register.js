import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container, Content, Button, Text, ListItem,
  Form, Spinner, View, Thumbnail, Item, Input, Right, Radio
} from 'native-base';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
//import RNGooglePlacePicker from 'react-native-google-place-picker';
import { adduserredux, resetLoging } from '../service/actions';
import styles from '../styles/forms';
import renderInput from '../Components/renderInputs';

const validate = values => {
  const error = {};

  let un = values.username;
  let fn = values.name;
  let ema = values.email;

  let pwd = values.password;

  if (pwd === undefined) {
    pwd = '';
  }

  if (pwd.length < 6 && pwd !== '') {
    error.password = 'too short';
  }
  if (pwd.length > 12 && pwd !== '') {
    error.password = 'too long';
  }

  if (values.email === undefined) {
    ema = '';
  }
  if (values.username === undefined) {
    un = '';
  }
  if (values.name === undefined) {
    fn = '';
  }
  if (ema.length < 8 && ema !== '') {
    error.email = 'too short';
  }
  if (!ema.includes('@') && ema !== '') {
    error.email = '@ not included';
  }
  if (un.length > 10) {
    error.username = 'max 10 characters';
  }
  if (un.length < 1) {
    error.username = 'Please enter username';
  }
  if (fn.length < 1) {
    error.name = 'Please enter User full Name';
  }
  return error;
};


const phoneFormatter = (number) => {
  if (!number) return '';
  // NNN-NNN-NNNN
  const splitter = /.{1,3}/g;
  number = number.substring(0, 10);
  return number.substring(0, 7).match(splitter).join('-') + number.substring(7);
};

/**
 * Remove dashes added by the formatter. We want to store phones as plain numbers
 */
const phoneParser = (number) => number ? number.replace(/-/g, '') : '';

class Register extends Component {
  constructor(props) {
    super(props);
    // renderInput = renderInput.bind(this);
    //let identfy = this.props.type === 'trade' ? 'تاجر' : 'ورشة';
    // this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      image: {},
      loc: { latitude: undefined, longitude: undefined },
      imageok: false,
      typear: '',
      loaded: false,
      type: this.props.types,

    };
  }


  componentDidMount() {
    console.log('Component mounted');
    this.props.reset();
    this.props.resetLoging(null);
    if (this.props.user.token !== null) {
      console.log('Logging User Has Token');
      Actions.auth();
    }
    console.log('Type ####', this.props.types);
    if (this.props.types === 'Traders') {
      this.setState({ typear: 'متجر', types: 'Trade' });
    } else if (this.props.types === 'Workshop') {
      this.setState({ typear: 'ورشة', types: 'Workshop' });
    } else {
      this.setState({ typear: 'جديد', types: 'Individual' });
    }
  }

  componentDidUpdate() {
    if (this.props.user.logged === true) {
      console.log('Logging User Has Token');
      Actions.auth();
    }
  }

  locationPicker() {/*
    RNGooglePlacePicker.show((response) => {
      if (response.didCancel) {
        console.log('User cancelled GooglePlacePicker');
      } else if (response.error) {
        console.log('GooglePlacePicker Error: ', response.error);
      } else {
        console.log('GooglePlacePicker Place: ', response);
        this.setState({
          loc: response
        });
      }
    });*/
  }

  pickerShow = () => {
    // iPhone/Android
    DocumentPicker.show({
      filetype: [DocumentPickerUtil.images()],
    }, (error, res) => {
      console.log("Error", error);
      if (error === null) {
        console.log(
          res.uri,
          res.type, // mime type
          res.fileName,
          res.fileSize
        );
        this.setState({
          image: res,
          imageok: true
        });
      } else {
        this.setState({
          image: {},
          imageok: false
        });
      }
    });
    // iPad
    /*
    const { pageX, pageY } = event.nativeEvent;
  
    DocumentPicker.show({
      top: pageY,
      left: pageX,
      filetype: ['public.image'],
    }, (error, url) => {
      alert(url);
    });
  */
  };
  renderButton() {
    if (this.props.user.logging !== undefined) {
      if (this.props.user.logging === true) {
        return (
          <Spinner />
        );
      }
    }
    return (
      <Button
        Light rounded block style={styles.buttonStyle}
        onPress={this.props.handleSubmit(this.props.submitin).bind(this.state)}
      >
        <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > تسجيل جديد</Text>
      </Button>
    );
  }

  render() {
    const { latitude, longitude } = this.state.loc;

    const { typear } = this.state;
    if (this.state.types === 'Individual') {
      return (
        <Container isRTL style={{ backgroundColor: '#E9EAEB' }} >
          {/* <KeyboardAvoidingView> */}
          {/* <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}> */}

          <Content padder style={styles.content} >
            <Text style={styles.titleStyle} > تسجيل حساب جديد </Text>

            <Form
              style={styles.container}
              onSubmit={(e) => { this.props.onSubmit }}
            >
              <Field key='3' name={'name'} im='lock' label='الاسم ' component={renderInput.bind(this)} />
              <Field
                key='1' name={'user_phone'} im='phone'
                label='رقم الجوال' component={renderInput.bind(this)}
                kp='numeric'
                type='phone'
              />
              <Field key='2' name={'password'} im='lock' label='كلمة المرور' secure component={renderInput.bind(this)} />
              <Text style={{ color: 'red', alignContent: 'center', flex: 1 }} >{this.props.user.errors.message}</Text>
              {this.renderButton()}
            </Form>
            <Button
              transparent rounded block style={styles.buttonStyleTransperent}
              onPress={() => Actions.Loginauth()}
            >
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, color: 'black' }} > اذا كان لديك حساب يالفعل الرجاء
          <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, color: 'red' }} >  تسجيل الدخول</Text>
              </Text>
            </Button>
          </Content>
          {/* </KeyboardAvoidingView> */}
        </Container >
      );
    }
    return (
      <Container isRTL style={{ backgroundColor: '#E9EAEB' }} >
        {/* <KeyboardAvoidingView> */}
        {/* <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}> */}
        <Content padder style={styles.content} >
          <Text style={styles.titleStyle} >
            تسجيل حساب {this.props.types === 'Traders' ? 'تاجر' : typear}
          </Text>

          <Form style={styles.container}>
            <Field
              key='1' name={'user_phone'} im='phone'
              label='رقم الجوال' type='text' component={renderInput.bind(this)}
              kp='numeric'
              type='phone'
            />

            <Field
              key='2' name={'password'} im='lock'
              label='كلمة المرور' secure type='text'
              component={renderInput.bind(this)}
            />

            <Field
              key='3' name={'name'} im='shop'
              label={'اسم ال' + typear} type='text'
              component={renderInput.bind(this)}
            />

            <Item
              onPress={() => this.pickerShow()}
              rounded style={styles.item}
            >
              {
                this.state.imageok ?
                  (<Thumbnail square source={{ uri: this.state.image.uri }} />)
                  : (<View />)
              }
              <Input
                key='20' name={'image'} im='lock'
                disabled
                label='ارفاق الاستمارة'
                style={styles.inputStyle}
                placeholder={' صورة لل' + typear}
              />

              <Thumbnail
                square
                source={require('../assets/images/camera.png')}
                style={styles.iconImageBig}
              />
            </Item>

            <Field
              key='4' name={'Description'} im='note'
              label={'وصف ال' + typear} type='multi'
              component={renderInput.bind(this)}
            />


            <Item
              rounded style={styles.item}
              onPress={() => this.locationPicker()}
            >
              <Input
                key='21' name={'image'} im='lock'
                disabled
                label='ارفاق العنوان'
                style={styles.inputStyle}
                placeholder={'موقع ال' + typear}
              >
                <Text>{latitude ? `${latitude} ${longitude}` : ''}</Text>
              </Input>

              <Thumbnail
                square
                source={require('../assets/images/locat.png')}
                style={styles.iconImageBig}
              />
            </Item>

            {this.props.types === 'Traders' ?
              (
                <Item
                  rounded style={[styles.itemMulti, {}]}
                >
                  <View
                    style={{
                      flex: 9,
                      flexDirection: 'column',
                      borderRightWidth: 1,
                      borderColor: '#C4C4C4',
                    }}
                  >
                    <Input
                      key='21' name={'loc'} im='lock'
                      disabled
                      label='ارفاق الاستمارة'
                      style={[styles.inputStyle, { flex: 1, borderColor: '#fff', }]}
                      placeholder={'تخصيص ال' + typear}
                    />
                    <View style={{ flex: 1, flexDirection: 'row-reverse', }}>
                      <Radio
                        style={{ marginRight: 20, }}
                        onPress={() => this.setState({ AccountType: 'new' })}
                        selected={this.state.AccountType === 'new'}
                      />
                      <Text style={[styles.raidoStyle, {}]} >تاجر قطع غيار جديدة</Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row-reverse', }}>
                      <Radio
                        style={{ marginRight: 20, }}
                        onPress={() => this.setState({ AccountType: 'used' })}
                        selected={this.state.AccountType === 'used'}
                      />
                      <Text style={[styles.raidoStyle, {}]} >تاجر قطع غيار مستعملة</Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row-reverse', }}>
                      <Radio
                        style={{ marginRight: 20, }}
                        onPress={() => this.setState({ AccountType: 'motorcycle' })}
                        selected={this.state.AccountType === 'motorcycle'}
                      />
                      <Text style={[styles.raidoStyle, {}]} >تاجر دراجات نارية</Text>
                    </View>
                  </View>

                  <Thumbnail
                    square
                    source={require('../assets/images/locat.png')}
                    style={styles.iconImageBig}
                  />

                </Item>
              ) : (
                <View />
              )
            }

            <Text style={{ color: 'red', alignContent: 'center', flex: 1 }} >{this.props.user.errors.message}</Text>
            {this.renderButton()}
          </Form>
        </Content>
        {/* </KeyboardAvoidingView> */}
      </Container >
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.users
  };
}

const mapDispatchToProps = (dispatch, getState) => {
  return bindActionCreators({
    submitin: adduserredux,
    resetLoging
  }, dispatch, getState);
};

//Register = connect(mapStateToProps, mapDispatchToProps, null, {withRef: true })(Register);
export default reduxForm({
  form: 'registerUser', validate
})(connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Register));
