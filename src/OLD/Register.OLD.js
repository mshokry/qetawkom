import React, { Component } from 'react';
import { Linking, CameraRoll, PermissionsAndroid } from 'react-native';
// import { ImagePicker } from 'expo';
import {
  Input, Form, Item, Icon, Textarea,
  Button, Text, Thumbnail, Container, Content
} from 'native-base';
import { create } from 'apisauce';
import styles from './Components/style';
import HeaderMain from './Components/HeaderMain';
// import RNGooglePlacePicker from 'react-native-google-place-picker';
// create api. 
const api = create({
  baseURL: 'http://localhost:3000',
});

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      types: '', phone: '', password: '',
      name: '', desc: '',
      image: [], location: '', imageok: false,
      typear: '',
      loaded: false,
    };
    this.setState(previousState => { return { types: this.props.types }; });
    console.log(this.props.types);
    console.log('then');

    if (this.props.types === 'Trade') {
      console.log('Trader');
      this.setState({ typesar: 'تاجر', types: 'Trade' });
    } else if (this.props.types === 'Workshop') {
      this.setState({ typear: 'ورشة', types: 'Workshop' });
    } else {
      this.setState({ typear: 'جديد', types: 'Individual' });
    }
    console.log(this.state.types);
    console.log(this.state.typear);
  }

  onPressButton() {
    //driving d   //walking walking   //bicycling bicycle   //transit transit
    this.startNavigation('google.navigation:q=home&mode=d');
  }

  imageRender() {
    if (this.state.imageok) {
      return <Thumbnail square source={{ uri: this.state.image }} />;
    }
  }

  upload() {
    // create formdata
    const data = new FormData();
    data.append('username', this.state.phone);
    data.append('password', this.state.password);
    data.append('name', this.state.name);
    data.append('Description', this.state.desc);
    data.append('location', this.state.location);
    this.state.image.forEach((photo, index) => {
      data.append('photos', {
        uri: photo.uri,
        type: 'image/jpeg',
        name: 'image' + index
      });
    });
    // post your data.
    api.post('/array', data, {
      onUploadProgress: (e) => {
        console.log(e)
        const progress = e.loaded / e.total;
        console.log(progress);
        this.setState({
          progress: progress
        });
      }
    })
      .then((res) => console.log(res));
  }

  startNavigation(url) {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\'t know how to open URI: ' + url);
      }
    });
  }

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          'title': 'Cool Photo App Camera Permission',
          'message': 'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.'
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  _pickImage = async () => {
    this.requestCameraPermission();
    /*
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri, imageok: true });
    } else {
      this.setState({ imageok: false });
    }
    */
    CameraRoll.getPhotos({
      first: 20,
      assetType: 'Photos',
    })
      .then(r => {
        this.setState({ image: r.edges, imageok: true });
      })
      .catch((err) => {
        //Error Loading Images
        this.setState({ imageok: false });
      });
  };

  render() {
    if (this.state.types === 'Individual') {
      return (
        <Container isRTL >
          <HeaderMain title="##قطعكم##" />
          <Content padder style={styles.content} >
            <Form style={styles.container}>
              <Item rounded style={styles.item} >
                <Input
                  placeholder='رقم الجوال'
                  style={styles.inputStyle}
                  onChangeText={text => this.setState({ phone: text })}
                  value={this.state.phone}
                />
                {/* <Label style={styles.labelStyle} > رقم الجوال </Label> */}
                <Icon style={styles.iconStyle} type="FontAwesome" name='mobile' />
              </Item>

              <Item rounded style={styles.item} last >
                <Input
                  placeholder='كلمة المرور'
                  style={styles.inputStyle}
                  secureTextEntry
                  onChangeText={text => this.setState({ password: text })}
                  value={this.state.password}
                />
                {/* <Label style={styles.labelStyle} > كلمة المرور </Label> */}
                <Icon style={styles.iconStyle} type="Ionicons" name='ios-lock-outline' />
              </Item>
              <Button Light rounded block style={{ padding: 10, marginTop: 10 }} >
                <Text>تسجيل</Text>
              </Button>
            </Form >
          </Content>
        </Container>
      );
    }
    return (
      <Container isRTL >
        {/* <HeaderMain title="##قطعكم##" /> */}
        <Content padder style={styles.content} >
          <Form style={styles.container}>
            <Text style={styles.titleStyle} > تسجيل حساب {this.props.typear} </Text>

            <Item rounded style={styles.item} >
              <Input
                placeholder='رقم الجوال'
                style={styles.inputStyle}
                onChangeText={text => this.setState({ phone: text })}
                value={this.state.phone}
              />
              {/* <Label style={styles.labelStyle} > رقم الجوال </Label> */}
              <Icon style={styles.iconStyle} type="FontAwesome" name='mobile' />
            </Item>

            <Item rounded style={styles.item} >
              <Input
                placeholder='كلمة المرور'
                style={styles.inputStyle}
                secureTextEntry
                onChangeText={text => this.setState({ password: text })}
                value={this.state.password}
              />
              {/* <Label style={styles.labelStyle} > كلمة المرور </Label> */}
              <Icon style={styles.iconStyle} type="Ionicons" name='ios-lock-outline' />
            </Item>

            <Item rounded style={styles.item}>
              <Input
                placeholder={' أسم ' + this.state.typear}
                style={styles.inputStyle}
                onChangeText={text => this.setState({ name: text })}
                value={this.state.name}
              />
              {/* <Label style={styles.labelStyle} > أسم {this.state.typear} </Label> */}
              <Icon style={styles.iconStyle} type="MaterialIcons" name='location-city' />
            </Item>

            <Item onPress={this._pickImage} rounded style={styles.item}>
              {this.imageRender()}
              {/* <Button style={{ flex: 1, }} block transparent onPress={this._pickImage} /> */}
              <Input placeholder={'صورة' + this.state.typear} style={styles.inputStyle} disabled />
              {/* <Label style={styles.labelStyle} > صورة {this.state.typear} </Label> */}
              <Icon style={styles.iconStyle} type="Entypo" name='camera' />
            </Item>

            <Item rounded style={styles.item}>
              <Textarea
                style={styles.inputStyle}
                // style={{ flex: 1, }}
                placeholder={'وصف' + this.state.typear}
                rowSpan={5}
                onChangeText={text => this.setState({ desc: text })}
                value={this.state.desc}
              />
              {/* <Label style={styles.labelStyle} > وصف {this.state.typear} </Label> */}
              <Icon style={styles.iconStyle} type="MaterialIcons" name='description' />
            </Item>

            <Item
              onPress={this.onPressButton.bind(this)}
              rounded style={styles.item} Last
            >
              {/* <Button style={{ flex: 1, }} full onPress={this.onPressButton.bind(this)} /> */}
              <Input placeholder={'موقع' + this.state.typear} style={styles.inputStyle} disabled />
              {/* <Label style={styles.labelStyle} > موقع {this.state.typear} </Label> */}
              <Icon style={styles.iconStyle} type="FontAwesome" name='map-marker' />
            </Item>

            <Button Light rounded block style={styles.buttonStyle} >
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, }} > تسجيل الآن</Text>
            </Button>
          </Form >
        </Content>
      </Container>
    );
  }
}

export default Register;
