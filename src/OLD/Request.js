import React, { PureComponent } from 'react';
import { FlatList, LayoutAnimation } from 'react-native';
import { List, ListItem, Text, Body } from 'native-base';
import { connect } from 'react-redux';
import styles from './style';
import * as actions from '../Actions';


class MyListItem extends React.PureComponent {
  renderSelected(id) {
    const { libraries, expanded } = this.props;
    if (expanded) {
      console.log('renderSel');
      console.log(this.props.selectedRequest);
      console.log(id);
      return (
        <Text note style={styles.noteText} >{libraries.description}</Text>
      );
    }
  }
  render() {
    const { item } = this.props;
    console.log(item.id);
    console.log(this.props.selectedRequest);
    return (
      <ListItem >
        <Body >
          <Text style={styles.baseText}>المكتبه {item.title} </Text>
          <Text note style={styles.noteText} >تاريخ 5/55/55555</Text>
          {this.renderSelected(item.id)}
        </Body>
      </ListItem>
    );
  }
}

class Requests extends PureComponent {

  componentWillUpdate() {
    LayoutAnimation.spring();
  }

  renderItem(item) {
    return (
      <ListItem key={item.id} onPress={() => this.props.selectLibrary(item.id)}>
        <Body >
          <Text style={styles.baseText}>المكتبه {item.title} </Text>
          <Text note style={styles.noteText} >تاريخ 5/55/55555</Text>
          {this.renderSelected(item.id)}
        </Body>
      </ListItem>
    );
  }

  render() {
    console.log('Requeaaa');
    console.log(this.props);
    return (
      // <List
      //   dataArray={this.props.libraries}
      //   renderRow={(lib) =>
      //     <ListItem key={lib.id} onPress={() => this.props.selectLibrary(lib.id)}>
      //       <Body>
      //         <Text style={styles.baseText}>المكتبه {lib.title} </Text>
      //         <Text note style={styles.noteText} >تاريخ 5/55/55555</Text>
      //         {this.renderSelected(lib.id)}
      //       </Body>
      //     </ListItem>
      //   }
      // />
      <FlatList
        data={this.props.libraries}

        renderItem={({ item }) =>
          <MyListItem
            id={item.id}
            onPressItem={() => this.props.selectLibrary(item.id)}
            item={item}
            selectLibrary={this.props.selectLibrary}
            expanded={item.id === this.props.selectedRequest}
          />}
        keyExtractor={(item, index) => item.id.toString()}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  // console.log(ownProps);
  return {
    libraries: state.libraries,
    selectedID: state.selectedRequest,
    expanded: state.libraries.id === state.selectedRequest
  };
};

export default connect(mapStateToProps, actions)(Requests);
// export default connect(mapStateToProps, actions)(Requests,MyListItem);
