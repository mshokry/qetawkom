import React, { Component } from 'react';
import { Image, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Left, Right, Body, Button, Text, Footer, Icon, Header, Badge } from 'native-base';
import styles from '../styles/headerStyle';

class FooterMain extends Component {
  render() {
    return (
      // <Container>
      <Footer
        style={{ backgroundColor: 0XE9EAEBFF, height: 55, marginBottom: -10 }}
        noShadow
      >
        <Button
          onPress={() => Actions.Requests()}
          style={{
            flex: 4,
            backgroundColor: '#0071D0',
            borderTopRightRadius: 30,
            height: 45
          }}
        >
          <View style={{ flexDirection: 'row-reverse', }} >
            <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, }}>طلباتي</Text>
            {/* <Icon name="apps" /> */}
            <Image
              source={require('../assets/images/shopping.png')}
              style={{
                width: 35, height: 35, resizeMode: 'contain', tintColor: '#fff', marginLeft: 20,
              }}
            />
          </View>

        </Button>
        <Button
          transparent
          style={{ backgroundColor: '#E9EAEB', flex: 3, }}
        >
          <Image
            source={require('../assets/images/blogo.png')}
            style={{
              flex: 1, width: 45, height: 45, resizeMode: 'contain', alignContent: 'center', tintColor: '#000', marginTop: 5,
            }}
          />
        </Button>
        <Button
          style={{
            flex: 4,
            backgroundColor: '#0071D0',
            borderTopLeftRadius: 20,
            height: 45
          }}
          onPress={() => Actions.myInfo()}
        >
          <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, }}>حسابي</Text>
          {/* <Icon name="person" /> */}
          <View style={{ flexDirection: 'row-reverse', }} >
          <Image
            source={require('../assets/images/profile.png')}
            style={{
              width: 35, height: 35, resizeMode: 'contain', tintColor: '#fff', marginRight: 30,
            }}
          />
          </View>
        </Button>
      </Footer>
    );
  }
}

export default FooterMain;


/* <Footer>
        <FooterTab> 
<Button badge >
  <Badge><Text>2</Text></Badge>
  <Icon name="apps" />
  <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, }}>طلباتي</Text>
</Button>
  <Button style={styles.headercontent} >
    <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 27, color: '#FFF' }}> قطعكم </Text>
    <Image
      source={require('../assets/images/blogo.png')}
      style={{ width: 45, height: 45, resizeMode: 'contain', tintColor: '#FFF', marginTop: 5, }}
    />
  </Button>
  <Button>
    <Icon name="person" />
    <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 18, }}>حسابي</Text>
  </Button>
        </FooterTab> 
        </Footer >


        */