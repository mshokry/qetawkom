import React, { Component } from 'react';
import {
	TextInput
} from 'react-native';
import {
	Item, Input, Thumbnail, Textarea, Picker,
	Text
} from 'native-base';
import styles from '../styles/forms';


export default class renderInput extends React.PureComponent {
	constructor(props) {
		super(props);
	}

	focus() {
		this.input.focus();
	}
	render() {
		const {
			input, label, type, keyx, secure,
			im, req, kp,
			inputProps, update, name,
			touched, error, warning,
			containerStyle,
			...props
		} = this.props;
		//console.log('this.Props :', this.props);
		let image;
		switch (im) {
			case 'lock': image = require('../assets/images/lock.png'); break;
			case 'locat': image = require('../assets/images/locat.png'); break;
			case 'edit': image = require('../assets/images/edit.png'); break;
			case 'profile': image = require('../assets/images/profile.png'); break;
			case 'mech': image = require('../assets/images/mech.png'); break;
			case 'clint': image = require('../assets/images/clint.png'); break;
			case 'shop': image = require('../assets/images/shop.png'); break;
			case 'camera': image = require('../assets/images/camera.png'); break;
			case 'phone': image = require('../assets/images/phone.png'); break;
			case 'note': image = require('../assets/images/note.png'); break;
			case 'price': image = require('../assets/images/ic_attach_money_black_24dp.png'); break;
			case 'date': image = require('../assets/images/event.png'); break;
			default: image = require('../assets/images/note.png'); break;
		}

		let hasError = false;
		if (error !== undefined && touched) {
			hasError = true;
		}
		if ((this.props.value === undefined || this.props.value === '') && touched && req) {
			hasError = true;
		}
		if (type === 'multi') {
			return (
				<Item error={hasError} key={keyx} rounded style={[styles.itemMultiInp, containerStyle]}>
					<Textarea
						style={styles.inputStyle}
						{...input}
						placeholder={label}
						rowSpan={5}
						{...props}
						onChangeText={(e) => update(name, e)}
					/>
					<Thumbnail
						square
						source={image}
						style={styles.iconImageBig}
					/>
				</Item>
			);
		}
		if (type === 'picker') {
			const { input: { onChange, value, ...inputProps }, children, ...pickerProps } = this.props;
			return (
				<Picker
					style={styles.item}
					selectedValue={value}
					onValueChange={value => onChange(value)}
					{...inputProps}
					{...pickerProps}
				>
					{children}
				</Picker>
			);
		}
		if (type === 'phone') {
			return (
				<Item error={hasError} key={keyx} rounded style={[styles.item, containerStyle]}>
					{/* <Label>{label}    <Text style={{ color: 'red' }}>    {error}</Text></Label> */}
					<Text
						style={{ fontFamily: 'Cairo-Light', fontSize: 20, marginLeft: 10, }}
					>00966</Text>
					<Input
						// {...input} 
						{...inputProps}

						style={styles.inputStyle}
						secureTextEntry={secure}
						keyboardType={kp ? kp : 'default'}
						placeholder={label}
						keyboardShouldPersistTaps='always'
						{...props}
						onChangeText={(e) => update(name, e)}
					/>
					<Thumbnail
						square
						source={image}
						style={[styles.iconImageBig, { flex: 3, }]}
					/>
					{/* {hasError ? <Icon name='close-circle' /> : <Icon />} */}
				</Item >
			);
		}
		return (
			<Item error={hasError} key={keyx} rounded style={styles.item}>
				{/* <Label>{label}    <Text style={{ color: 'red' }}>    {error}</Text></Label> */}
				<Input
					{...input} key="1{keyx}"
					ref={inputs => (this.input = inputs)}
					style={styles.inputStyle}
					secureTextEntry={secure}
					keyboardType={kp ? kp : 'default'}
					placeholder={label}
					{...props}
					onChangeText={(e) => update(name, e)}

				// value={props.value}
				/>
				<Thumbnail
					square
					source={image}
					style={styles.iconImageBig}
				/>
				{/* {hasError ? <Icon name='close-circle' /> : <Icon />} */}
			</Item>
		);
	}
}
