import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Button, Text, View, Thumbnail } from 'native-base';
import styles from '../styles/forms';

const MainImport = (props) => {
	return (
		<View style={{ flex: 1, }}>

			<Button
				full large rounded style={styles.MultibtnStyle}
				onPress={() => Actions.Requests({ types: 'NewRequests' })}
			>
				<Text style={styles.buttonText}> طلبات لم يتم الرد عليها </Text>
				<Thumbnail square source={require('../assets/images/new_file.png')} style={styles.iconImageBig} />
			</Button>

			<Button
				full large rounded style={styles.MultibtnStyle}
				onPress={() => Actions.Requests({ types: 'OldRequests' })}
			>
				<Text style={styles.buttonText}> طلبات تم الرد عليها </Text>
				<Thumbnail square source={require('../assets/images/edit.png')} style={styles.iconImageBig} />
			</Button>

			<Button
				full large rounded style={styles.MultibtnStyle}
				onPress={() => Actions.Requests({ types: 'new' })}
			>
				<Text style={styles.buttonText}> طلبات الاستيراد </Text>
				<Thumbnail square source={require('../assets/images/plane.png')} style={styles.iconImageBig} />
			</Button>

			<Button
				full large rounded style={styles.MultibtnStyle}
				onPress={() => props.logout()}
			>
				<Text style={[styles.buttonText, { color: 'red', borderColor: 'red' }]}> تسجيل الخروج </Text>
				<Thumbnail
					square source={require('../assets/images/logout.png')}
					style={[styles.iconImageBig, { tintColor: 'red' }]}
				/>
			</Button>

		</View>

	);
};

export default MainImport;
