import React, { Component } from 'react';
import { Image } from 'react-native';
import { Header, Left, Right, Body, Button, Text } from 'native-base';
import {Actions} from 'react-native-router-flux';
import styles from '../styles/headerStyle';

class HeaderMain extends Component {
  render() {
    return (
      // <Container>
      <Header 
        style={{ backgroundColor: 0XE9EAEBFF, height: 65, }} 
        androidStatusBarColor={0X0071D0FF}
        noShadow
      >
        <Left style={{ flex: 1, backgroundColor: '#E9EAEB', }} />
        <Body style={styles.headercontent}>
          <Button transparent onPress={() => Actions.pop()} >
            <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, color: '#FFF' }}> قطعكم </Text>
            <Image
              source={require('../assets/images/blogo.png')}
              style={{ width: 50, height: 50, resizeMode: 'contain', tintColor: '#FFF', marginTop: 5, }}
            />
          </Button>
        </Body>
        <Right style={{ flex: 1, backgroundColor: '#E9EAEB', }} />
      </Header>
    );
  }
}

export default HeaderMain;
