import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Button, Text, View, Thumbnail } from 'native-base';
import styles from '../styles/forms';


const MainUser = (props) => (
	<View style={{ flex: 1, }}>
	<Button
		full large rounded style={styles.MultibtnStyle}
		onPress={() => Actions.partRequest({ types: 'new' })}
	>
		<Text style={styles.buttonText}> طلب قطعة غيار جديده </Text>
		{/* <Icon style={styles.iconStyleBig} type="FontAwesome" name='car' /> */}
		<Thumbnail square source={require('../assets/images/set.png')} style={styles.iconImageBig} />
	</Button>

	<Button
		full large rounded style={styles.MultibtnStyle}
		onPress={() => Actions.partRequest({ types: 'used' })}
	>
		<Text style={styles.buttonText}> طلب قطعة غيار مستعملة </Text>
		<Thumbnail square source={require('../assets/images/tsettin.png')} style={styles.iconImageBig} />
	</Button>

	<Button
		full large rounded style={styles.MultibtnStyle}
		onPress={() => Actions.Import({ types: 'Workshop' })}
	>
		<Text style={styles.buttonText}> إستيراد قطعة غيار </Text>
		<Thumbnail square source={require('../assets/images/plane.png')} style={styles.iconImageBig} />
	</Button>

	<Button
		full large rounded style={styles.MultibtnStyle}
		onPress={() => Actions.partRequest({ types: 'motor' })}
	>
		<Text style={styles.buttonText}> طلب قطعة غيار دراجة نارية </Text>
		<Thumbnail square source={require('../assets/images/motorcycle.png')} style={styles.iconImageBig} />
	</Button>

	<Button
		full large rounded style={styles.MultibtnStyle}
		onPress={() => props.logout()}
	>
		<Text style={[styles.buttonText, { color: 'red', borderColor: 'red' }]}> تسجيل الخروج </Text>
		<Thumbnail
			square source={require('../assets/images/logout.png')}
			style={[styles.iconImageBig, { tintColor: 'red' }]}
		/>
	</Button>
</View>
);

export default MainUser;
