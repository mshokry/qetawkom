import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Button, Text, View, Thumbnail } from 'native-base';
import styles from '../styles/forms';


const SideBar = (props) => (
  <View style={{ flex: 1, }}>
      <Text >
        Control Panel
        </Text>
      <Button
        onPress={() => {
          this.props.closeDrawer();
        }}
        text="Close Drawer"
      />
  </View>
);

export default SideBar;
