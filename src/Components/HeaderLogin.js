import React, { Component } from 'react';
import { Image, View } from 'react-native';
import { Header, Left, Right, Body, Button, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from '../styles/headerStyle';

class HeaderLogin extends Component {
  render() {
    return (
      <Header
        style={{
           backgroundColor: 0XE9EAEBFF, height: 65, paddingLeft: 0, paddingRight: 0, 
        }}
        androidStatusBarColor={0X0071D0FF}
        noShadow
      >
        <Left style={styles.headerleft} >
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginBottom: 10 }} >
            <Button transparent onPress={() => Actions.Loginauth()} >
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, color: '#FFF' }}>دخول</Text>
            </Button>
            <Button transparent>
            <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, color: '#FFF' }}>/</Text>
            </Button>
            <Button transparent onPress={() => Actions.ToRegister()} >
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, color: '#FFF' }}>تسجيل</Text>
            </Button>
          </View>
        </Left>
        <Body style={styles.headercenter} />
        <Right style={styles.headerright} >
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', alignSelf: 'center',marginLeft:20,alignContent:'center',marginBottom: 10 }} >
            <Button transparent onPress={() => Actions.pop() } >
              <Image
                source={require('../assets/images/blogo.png')}
                style={{ width: 40, height: 40, resizeMode: 'contain', tintColor: '#FFF', marginTop: 5, }}
              />
              <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 20, color: '#FFF' }}> قطعكم </Text>
            </Button>
          </View>

        </Right>
      </Header>
    );
  }
}

export default HeaderLogin;
