import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Button, Text, View, Thumbnail } from 'native-base';
import styles from '../styles/forms';
/*
new used  motorcycle
*/

const MainTrade = (props) => {
	let typear = '';
	if (props.user.AccountType === 'motorcycle') {
		typear = 'للدراجات النارية';
	} else if (props.user.AccountType === 'Workshop') {
		typear = 'المستعملة';
	} else {
		typear = 'الجديدة';
	}
	return (
		<View style={{ flex: 1, }}>
			<Button
				full large rounded style={styles.MultibtnStyle}
				onPress={() => Actions.Requests({ types: 'new' })}
			>
				<Text style={styles.buttonText}> طلبات قطع الغيار {typear} </Text>
				<Thumbnail square source={require('../assets/images/set.png')} style={styles.iconImageBig} />
			</Button>

			<Button
				full large rounded style={styles.MultibtnStyle}
				onPress={() => Actions.Requests({ types: 'NewRequests' })}
			>
				<Text style={styles.buttonText}> طلبات لم يتم الرد عليها </Text>
				<Thumbnail square source={require('../assets/images/new_file.png')} style={styles.iconImageBig} />
			</Button>

			<Button
				full large rounded style={styles.MultibtnStyle}
				onPress={() => Actions.Requests({ types: 'OldRequests' })}
			>
				<Text style={styles.buttonText}> طلبات تم الرد عليها </Text>
				<Thumbnail square source={require('../assets/images/edit.png')} style={styles.iconImageBig} />
			</Button>

			<Button
				full large rounded style={styles.MultibtnStyle}
				onPress={() => props.logout()}
			>
				<Text style={[styles.buttonText, { color: 'red', borderColor: 'red' }]}> تسجيل الخروج </Text>
				<Thumbnail
					square source={require('../assets/images/logout.png')}
					style={[styles.iconImageBig, { tintColor: 'red' }]}
				/>
			</Button>

		</View>

	);
};

export default MainTrade;
