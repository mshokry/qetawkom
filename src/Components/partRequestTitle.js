import React, { Component } from 'react';
import { View, Text,Image } from 'react-native';
import styles from '../styles/forms';
export default class Title extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: this.props.type,
      import_image: this.props.country
    };
  }

  render() {
      switch (this.state.type) {
        case 'new':
          return (<Text style={styles.titleStyle} > طلب قطعة غيار جديدة </Text>);
        case 'used':
          return (<Text style={styles.titleStyle} > طلب قطعة غيار مستعملة </Text>);
        case 'import':
          switch (this.state.import_image) {
            case 'uae':
              return (
                <View style={{ alignItems: 'center', }}>
                  <Text style={styles.titleStyle} > استيراد قطعة غيار </Text>
                  <Image
                    source={require(`../assets/images/uae.png`)}
                    style={styles.iconImageTitle}
                  />
                </View>
              );
            case 'usa_flag':
              return (
                <View style={{ alignItems: 'center', }}>
                  <Text style={styles.titleStyle} > استيراد قطعة غيار </Text>
                  <Image
                    source={require(`../assets/images/usa_flag.png`)}
                    style={styles.iconImageTitle}
                  />
                </View>
              );
            default:
              return (
                <View style={{ alignItems: 'center', }}>
                  <Text style={styles.titleStyle} > استيراد قطعة غيار </Text>
                  <Image
                    source={require(`../assets/images/japan.png`)}
                    style={styles.iconImageTitle}
                  />
                </View>
              );
          }

        case 'motor':
          return (<Text style={styles.titleStyle} > طلب قطعة غيار دراجة نارية </Text>);
        default:
          return (<Text style={styles.titleStyle} > طلب قطعة غيار جديدة </Text>);
      }
  }
}
