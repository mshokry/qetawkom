import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    baseText: {
        fontFamily: 'Cairo-Regular',
        fontSize: 20,
        color: '#000',
        direction: 'rtl',
    },
    noteText: {
        fontFamily: 'Cairo-Light',
        fontSize: 16,
        color: '#000'
    },
    noteTextRed: {
        fontFamily: 'Cairo-Regular',
        fontSize: 18,
        color: '#FF0000'
    },
    noteTextGreen: {
        fontFamily: 'Cairo-Regular',
        fontSize: 18,
        color: '#00FF00'
    },
    content: {
        flex: 1,
        width: '95%',
        height: '100%',
        alignSelf: 'center',
    },
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'space-between',
        height: '100%'
    },
    titleStyle: {
        width: '70%',
        textAlign: 'center',
        alignSelf: 'center',
        borderBottomWidth: 1,
        borderColor: '#89898A',
        fontFamily: 'Cairo-Regular',
        fontSize: 22,
        height: 55,
        marginBottom: 10,
        marginTop: 10,
    },
    titleEmptyStyle: {
        flex: 1,
        width: '100%',
        textAlign: 'center',
        borderBottomWidth: 1,
        borderColor: '#89898A',
        fontFamily: 'Cairo-Regular',
        fontSize: 22,
        height: 30,
        marginBottom: 10,
        marginTop: 10,
    },
    iconImageTitle: {
        marginTop: -10,
        width: 80,
        height: 80,
        resizeMode: 'contain',
    },
    item: {
        padding: 5,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#FFF',
        height: 80,
    },
    itemMulti: {
        padding: 5,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#FFF',
    },
    inputStyle: {
        fontFamily: 'Cairo-Regular',
        fontSize: 20,
        borderRightWidth: 1,
        flex: 9,
        borderColor: '#C4C4C4',
        color: '#C4C4C4',
        paddingRight: 30,
        textAlign: 'right',
    },
    raidoStyle: {
        fontFamily: 'Cairo-Regular',
        fontSize: 20,
        color: '#000',
        textAlign: 'right',
        marginRight: 10,
        marginBottom: 10,
    },
    subinputStyle: {
        fontFamily: 'Cairo-Regular',
        fontSize: 20,
        flex: 4,
        borderColor: '#C4C4C4',
        color: '#000',
        textAlign: 'right',
        marginRight: 10,
    },
    pickerStyle: {
        borderRightWidth: 1,
        flex: 9,
        borderColor: '#C4C4C4',
        color: '#C4C4C4',
        paddingRight: 30,
        alignContent: 'center',
        alignSelf: 'center',
    },
    iconStyle: {
        fontSize: 30,
        color: '#0071D0',
        marginRight: 10,
        marginLeft: 10,
        flex: 1,
        textAlign: 'center',
    },
    iconImageStyle: {
        flex: 2,
        width: 24,
        height: 24,
        resizeMode: 'contain',
        tintColor: '#000'
    },
    iconImageBig: {
        flex: 2,
        width: 40,
        height: 40,
        resizeMode: 'contain',
        tintColor: '#000'
    },
    iconImageImport: {
        flex: 4,
        width: 80,
        height: 120,
        resizeMode: 'stretch',
        borderTopRightRadius: 40,
        borderBottomRightRadius: 40,
    },
    iconStyleBig: {
        fontSize: 32,
        marginRight: 7,
        color: '#000',
        flex: 1,
        textAlign: 'center',
    },
    labelStyle: {
        flex: 2,
        fontFamily: 'Cairo-Regular',
        color: '#0071D0',
    },
    buttonStyle: {
        padding: 10,
        marginTop: 10,
        backgroundColor: '#0071D0',
        height: 70
    },
    buttonStyleTransperent: {
        padding: 10,
        marginTop: 10,
        height: 55,
    },
    MultibtnStyle: {
        padding: 20,
        marginTop: 30,
        backgroundColor: '#FFF',
        shadowRadius: 4,
        alignContent: 'center',
        height: 70
    },
    buttonText: {
        fontFamily: 'Cairo-Light',
        fontSize: 18,
        color: '#000',
        flex: 9,
        borderRightWidth: 1,
        borderColor: '#000',
        height: 45,
        alignSelf: 'center',
        alignItems: 'center',
    },
    ImageStyle: {
        width: 200,
        height: 160,
        resizeMode: 'contain',
        borderRadius: 20,

    },
    dialog: {
        backgroundColor: 'green',
        borderRadius: 40,
        shadowRadius: 45,
        shadowColor: '#000',
    },
    PartV: {
        flexDirection: 'row-reverse' ,
    },
    PartT: {
        fontFamily: 'Cairo-Light',
        fontSize: 20,
        color: '#000',
        direction: 'rtl',
        flex: 1,
    },
    PartD: {
        fontFamily: 'Cairo-Light',
        fontSize: 20,
        color: 'green',
        direction: 'rtl',
        flex: 2,
        textAlign: 'right',
    },
});
