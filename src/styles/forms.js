import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    baseText: {
        fontFamily: 'Cairo-Regular',
        fontSize: 16,
        color: '#000',
        direction: 'rtl',
        textAlign: 'right',
        alignSelf: 'flex-end',
    },
    noteText: {
        fontFamily: 'Cairo-Light',
        fontSize: 16,
        textAlign: 'right',
        color: '#000'
    },
    noteTextRed: {
        fontFamily: 'Cairo-Regular',
        textAlign: 'right',
        fontSize: 14,
        color: '#FF0000'
    },
    noteTextGreen: {
        fontFamily: 'Cairo-Regular',
        textAlign: 'right',
        fontSize: 14,
        color: '#00FF00'
    },
    content: {
        flex: 1,
        width: '95%',
        height: '100%',
        alignSelf: 'center',
    },
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'space-between',
        height: '100%'
    },
    titleStyle: {
        width: '70%',
        textAlign: 'center',
        alignSelf: 'center',
        borderBottomWidth: 3,
        borderColor: '#89898A',
        fontFamily: 'Cairo-Regular',
        fontSize: 18,
        height: 50,
        marginBottom: 10,
        marginTop: 10,
    },
    titleEmptyStyle: {
        flex: 1,
        width: '100%',
        textAlign: 'center',
        borderBottomWidth: 1,
        borderColor: '#89898A',
        fontFamily: 'Cairo-Regular',
        fontSize: 22,
        height: 30,
        marginBottom: 10,
        marginTop: 10,
    },
    iconImageTitle: {
        marginTop: -10,
        width: 80,
        height: 80,
        resizeMode: 'contain',
    },
    item: {
        padding: 5,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#FFF',
        height: 65,
    },
    itemButton: {
        height: 75,
    },
    itemMultiInp: {
        padding: 5,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#FFF',
    },

    itemPick: {
        color: '#C4C4C4',
        height: 51,
        alignContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        flexDirection: 'row-reverse',
        width: '100%'
    },
    itemMulti: {
        padding: 5,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#FFF',
    },
    inputStyle: {
        fontFamily: 'Cairo-Regular',
        fontSize: 16,
        borderRightWidth: 1,
        flex: 9,
        borderColor: '#C4C4C4',
        color: '#C4C4C4',
        paddingRight: 30,
        textAlign: 'right',
    },
    PicInputStyle: {
        fontFamily: 'Cairo-Regular',
        fontSize: 16,
        borderRightWidth: 1,
        flex: 1,
        borderColor: '#C4C4C4',
        color: '#C4C4C4',
        paddingRight: 30,
        textAlign: 'right',
    },
    raidoStyle: {
        fontFamily: 'Cairo-Regular',
        fontSize: 15,
        color: '#000',
        textAlign: 'right',
        marginRight: 10,
        marginBottom: 10,
    },
    subinputStyle: {
        fontFamily: 'Cairo-Regular',
        fontSize: 16,
        flex: 4,
        borderColor: '#C4C4C4',
        color: '#000',
        textAlign: 'right',
        marginRight: 10,
    },
    pickerStyle: {
        borderRightWidth: 1,
        flex: 1,
        borderColor: '#C4C4C4',
        color: '#C4C4C4',
        paddingRight: 30,
        alignContent: 'center',
        alignSelf: 'center',
    },
    iconStyle: {
        fontSize: 30,
        color: '#0071D0',
        marginRight: 10,
        marginLeft: 10,
        flex: 1,
        textAlign: 'center',
    },
    iconImageStyle: {
        flex: 2,
        width: 24,
        height: 24,
        resizeMode: 'contain',
        tintColor: '#000'
    },
    iconImageBig: {
        flex: 2,
        width: 30,
        height: 30,
        resizeMode: 'contain',
        tintColor: '#000'
    },
    iconImageImport: {
        flex: 4,
        width: 80,
        height: 120,
        resizeMode: 'stretch',
        borderTopRightRadius: 40,
        borderBottomRightRadius: 40,
    },
    iconStyleBig: {
        fontSize: 32,
        marginRight: 7,
        color: '#000',
        flex: 1,
        textAlign: 'center',
    },
    labelStyle: {
        flex: 2,
        fontFamily: 'Cairo-Regular',
        color: '#0071D0',
    },
    buttonStyle: {
        padding: 10,
        marginTop: 10,
        backgroundColor: '#0071D0',
        height: 55,
        alignSelf: 'center',
    },
    buttonStyleTransperent: {
        padding: 10,
        marginTop: 10,
        height: 55,
        alignContent: 'center',
    },
    MultibtnStyle: {
        padding: 20,
        marginTop: 20,
        backgroundColor: '#FFF',
        shadowRadius: 4,
        alignContent: 'center',
        height: 60
    },
    buttonText: {
        fontFamily: 'Cairo-Light',
        fontSize: 18,
        color: '#000',
        flex: 9,
        borderRightWidth: 3,
        borderColor: '#000',
        height: 35,
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'right'
    },
    ImageStyle: {
        width: 200,
        height: 160,
        resizeMode: 'contain',
        borderRadius: 20,
    },
    dialog: {
        backgroundColor: 'green',
        borderRadius: 40,
        shadowRadius: 45,
        shadowColor: '#000',
    },
    PartV: {
        alignContent: 'flex-start',
        flexDirection: 'row-reverse',      
        justifyContent: 'flex-start'
    },
    PartT: {
        fontFamily: 'Cairo-Light',
        fontSize: 16,
        color: '#000',
        textAlign: 'right',
        flex: 1,
    },
    PartD: {
        fontFamily: 'Cairo-Light',
        fontSize: 16,
        color: 'green',
        flex: 2,
        textAlign: 'right',
    },
});
