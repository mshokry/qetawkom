import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    headercontent: {
        flex: 2,
        height: 50,
        alignSelf: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: '#0071D0',
        alignItems: 'center',
    },
    headerleft: {
        flex: 14,
        height: 50,
        alignSelf: 'center',
        borderBottomRightRadius: 20,
        backgroundColor: '#0071D0',
        alignItems: 'center',
    },
    headerright: {
        flex: 14,
        height: 50,
        alignSelf: 'center',
        borderBottomLeftRadius: 20,
        backgroundColor: '#0071D0',
        alignItems: 'center',
        paddingRight: 20,
    },
    headercenter: {
        flex: 1,
        height: 50,
        alignSelf: 'center',
        backgroundColor: '#E9EAEB',
        alignItems: 'center',

    },
});
