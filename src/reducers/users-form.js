import { reducer as formReducer } from 'redux-form';
// const reducers = {
export default formReducer.plugin({

  login: (state, action) => {
    switch (action.type) {
      case 'Login Error':
        return {
          ...state,
          logging: false,
          values: {
            ...state.values,
            password: undefined
          },
          registeredFields: {
            ...state.registeredFields,
            password: undefined
          },
          submitSucceeded: false,
          errors: {
            ...state.errors,
            password: 'Password Error###########'
          }

        };
      default:
        return state;
    }
  }
});
// export default reducers;
