import { combineReducers } from 'redux';
// import formReducer from './users-form';
// import { reducer as formReducer } from 'redux-form';
import users from './users';
import parts from './parts';

export default combineReducers({
    // libraries: LibraryReducers,
    // selectedRequest: RequestReducers,
    // auth: AuthReducer,
    // form: formReducer,
    users,
    parts
});
