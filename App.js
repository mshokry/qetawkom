
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';

import reducers from './src/reducers';
// import Register from './src/navigation/Register';
import RouterComponent from './src/Routers';

export default class App extends Component {
  render() {
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
        {/* <Container isRTL >
          <HeaderMain title="##قطعكم##" />
          <Content padder style={styles.content} > */}
          {/* <Root /> */}
        <RouterComponent />
        {/* </Content>
        </Container> */}
      </Provider>
    );
  }
}
